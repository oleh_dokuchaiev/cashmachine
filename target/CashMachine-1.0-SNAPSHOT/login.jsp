<html>
<%@ include file="WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="WEB-INF/jspf/directive/taglib.jspf" %>
<head>
    <fmt:message key="page.login.title" var="title" scope="page"/>
    <%@ include file="WEB-INF/jspf/head.jspf" %>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/style/single-block-page-style.css">
</head>
<body>
<%@include file="WEB-INF/jspf/header.jspf"%>
<fmt:message key="ui.login" var="localeLogIn"/>
<fmt:message key="ui.login.placeholder.userName" var="localeLoginPlaceholder"/>
<fmt:message key="ui.login.placeholder.password" var="localePasswordPlaceholder"/>
<div class="single-container" id="login-container">
    <h2 class="block-title">${localeLogIn}</h2>
    <hr>
    <form id="login-form" action="controller" method="post">
        <input type="hidden" name="command" value="login"/>
        <label for="login"><fmt:message key="ui.login.userName"/>:</label>
        <input type="text" class="user-input" id="login-input" name="login" placeholder="${localeLoginPlaceholder}"/>
        <label for="password"><fmt:message key="ui.login.password"/>:</label>
        <input type="password" class="user-input" id="pass-input" name="password" placeholder="${localePasswordPlaceholder}"/>
        <br/>
        <input type="submit" class="submit-button" value="${localeLogIn}">
    </form>
</div>
<%@ include file="WEB-INF/jspf/footer.jspf" %>
</body>
</html>