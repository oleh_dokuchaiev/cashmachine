<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="cl" uri="/WEB-INF/cookieLocale.tld" %>
<cl:GetCookieLocale/>
<fmt:setLocale value="${locale}" scope="session"/>
<fmt:requestEncoding value="UTF-8"/>
<fmt:setBundle basename="messages" scope="session"/>
<!DOCTYPE html>
<html>
<head>
    <fmt:message key="page.index.title" var="title"/>
    <%@ include file="WEB-INF/jspf/head.jspf" %>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/style/single-block-page-style.css">
</head>
<body>
<c:if test="${not empty user}">
    <c:url value="controller" var="openMain">
        <c:param name="command" value="openMain"/>
    </c:url>
    <c:redirect url="${openMain}"/>
</c:if>
<%@include file="WEB-INF/jspf/header.jspf" %>
<c:if test="${empty user and title ne 'Login'}">
    <div class="single-container" id="welcome-container">
        <div id="welcome-message">
            <h1 class="block-title">
                <fmt:message key="msg.index.welcome"/>
            </h1>
            <p><fmt:message key="msg.index.loginOffer"/></p>
            <hr>
            <p id="signature">CashMachine v1.0.0</p>
        </div>
    </div>
</c:if>
<%@ include file="WEB-INF/jspf/footer.jspf" %>
</body>
</html>
