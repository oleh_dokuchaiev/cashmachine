<%@ page isErrorPage="true" %>
<%@ page import="java.io.PrintWriter" %>
<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>

<html>
<head>
    <fmt:message key="page.error.title" var="title"/>
    <%@ include file="/WEB-INF/jspf/head.jspf" %>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/style/single-block-page-style.css">
</head>
<body id="error-page-body">
<c:set var="code" value="${requestScope['javax.servlet.error.status_code']}"/>
<c:set var="message" value="${requestScope['javax.servlet.error.message']}"/>
<div class="single-container" id="error-container">
    <h1 class="block-title" id="errorTitle"><fmt:message key="ui.error.title"/></h1>
    <hr>
    <c:if test="${not empty code}">
        <h2 id="errorCode">${code}</h2>
        <hr>
    </c:if>

    <c:if test="${not empty message}">
        <h3 class="error-message">${message}</h3>
        <hr>
    </c:if>

    <c:if test="${not empty requestScope.errorMessage}">
        <h3 class="error-message">${requestScope.errorMessage}</h3>
        <hr>
    </c:if>
    <c:url value="controller" var="openMain">
        <c:param name="command" value="openMain"/>
    </c:url>
    <button onclick="window.location.href='${openMain}';" class="submit-button"><fmt:message key="ui.error.homeButton"/></button>
</div>
<%@ include file="/WEB-INF/jspf/footer.jspf" %>
</body>
</html>