<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>

<html>
<body>
<div class="action-block" id="user-menu-block">
    <h3 class="block-title"><fmt:message key="ui.main.menuBlock.title"/></h3>
    <hr>
    <div class="menu-option" id="show-all-items-option">
        <c:url value="controller" var="showAllItems">
            <c:param name="command" value="showAllItems"/>
            <c:param name="page" value="1"/>
        </c:url>
        <form id="show-items-form" method="post" action="${showAllItems}">
            <button type="submit" class="menu-option-button" id="showItemsButton" form="show-items-form">
                <fmt:message key="ui.main.menuBlock.openStock"/>
            </button>
        </form>
    </div>
    <c:url value="controller" var="createItem">
        <c:param name="command" value="showNewItem"/>
    </c:url>
    <form id="create-item-form" method="post" action="${createItem}">
        <button type="submit" class="menu-option-button" id="createItemButton" form="create-item-form">
            <fmt:message key="ui.main.menuBlock.createItem"/>
        </button>
    </form>
</div>
</body>
</html>