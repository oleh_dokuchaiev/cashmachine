<html>
<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>
<head>
    <fmt:message key="page.main.title" var="title" scope="page"/>
    <%@include file="/WEB-INF/jspf/head.jspf" %>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/style/action-page-style.css">
</head>
<body>
<%@ include file="/WEB-INF/jspf/header.jspf" %>
<div class="main-container">
    <c:if test="${userRole != null}">
        <div class="info-block" id="userInfoBlock">
            <h3 class="block-title"><fmt:message key="ui.main.infoBlock.title"/></h3>
            <hr>
            <p class="info-string" id="Role">${user.role}</p>
            <hr>
            <p class="info-string"><fmt:message key="ui.main.infoBlock.login"/>: ${user.login}</p>
            <p class="info-string"><fmt:message key="ui.main.infoBlock.firstName"/>: ${user.firstName}</p>
            <p class="info-string"><fmt:message key="ui.main.infoBlock.lastName"/>: ${user.lastName}</p>
        </div>
        <c:choose>

            <c:when test="${userRole.name == 'cashier'}">
                <jsp:include page="cashier/cashier_menu.jsp"/>
            </c:when>

            <c:when test="${userRole.name == 'senior_cashier'}">
                <jsp:include page="senior-cashier/senior_cashier_menu.jsp"/>
            </c:when>

            <c:when test="${userRole.name == 'commodity_expert'}">
                <jsp:include page="commodity-expert/commodity_expert_menu.jsp"/>
            </c:when>

        </c:choose>

    </c:if>
</div>
<%@ include file="/WEB-INF/jspf/footer.jspf" %>
</body>
</html>