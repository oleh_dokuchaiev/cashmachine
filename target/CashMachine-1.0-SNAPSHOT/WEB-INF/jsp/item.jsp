<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>
<html>
<head>
    <fmt:message key="page.item.title" var="title" scope="page"/>
    <%@ include file="/WEB-INF/jspf/head.jspf" %>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/style/action-page-style.css">
</head>
<body>
<jsp:include page="/WEB-INF/jspf/header.jspf"/>
<div class="main-container">
    <div class="info-block">
        <h3 class="block-title"><fmt:message key="ui.item.infoBlock.title"/></h3>
        <hr>
        <p class="info-string"><fmt:message key="ui.item.infoBlock.id"/>: <c:out value="${item.id}"/></p>
        <p class="info-string"><fmt:message key="ui.item.infoBlock.name"/>: <c:out value="${item.name}"/></p>
        <p class="info-string"><fmt:message key="ui.item.infoBlock.stockAmount"/>: <c:out value="${item.amount}"/></p>
        <p class="info-string"><fmt:message key="ui.item.infoBlock.stockPrice"/>: <c:out value="${item.price}"/></p>
    </div>
    <fmt:message key="ui.submit" var="localeSubmit"/>
    <fmt:message key="ui.remove" var="localeRemove"/>
    <div class="user-menu-block">
        <h3 class="block-title"><fmt:message key="ui.main.menuBlock.title"/></h3>
        <hr>
        <c:choose>
            <c:when test="${commandName == 'addItemToOrder'}">
                <c:url value="controller" var="addItemCommand">
                    <c:param name="command" value="addItemToOrder"/>
                    <c:param name="itemId" value="${item.id}"/>
                </c:url>
                <form class="amount-input-form" method="post" action="${addItemCommand}">
                    <input type="number" name="itemAmount" step="0.001" min="0.000" placeholder="0.000">
                    <input type="submit" value="${localeSubmit}">
                </form>
            </c:when>

            <c:when test="${commandName == 'changeItemAmountInOrder'}">
                <c:url value="controller" var="updateOrderItemCommand">
                    <c:param name="command" value="changeItemAmountInOrder"/>
                    <c:param name="itemId" value="${item.id}"/>
                </c:url>
                <form class="amount-input-form" method="post" action="${updateOrderItemCommand}">
                    <fieldset>
                        <fieldset>
                            <label for="prevAmount"><fmt:message key="ui.item.updateAmount.label.currAmount"/>: </label>
                            <input type="number" name="prevAmount" value="${itemOrderAmount}" readonly>
                        </fieldset>
                        <label for="prevPrice"><fmt:message key="ui.item.updateAmount.label.currPrice"/>: </label>
                        <input type="number" name="prevPrice" value="${itemOrderPrice}" readonly>
                    </fieldset>
                    <input type="number" name="itemAmount" step="0.001" min="0.000">
                    <input type="submit" value="${localeSubmit}">
                </form>
            </c:when>

            <c:when test="${commandName == 'changeItemAmountInStorage'}">
                <c:url value="controller" var="updateStockItemCommand">
                    <c:param name="command" value="changeItemAmountInStorage"/>
                    <c:param name="itemId" value="${itemId}"/>
                    <c:param name="itemName" value="${itemName}"/>
                    <c:param name="itemPrice" value="${itemStockPrice}"/>
                </c:url>
                <form class="amount-input-form" method="post" action="${updateStockItemCommand}">
                    <input type="number" name="itemAmount" step="0.001" min="0.000">
                    <input type="submit" value="${localeSubmit}">
                </form>
            </c:when>

            <c:when test="${commandName == 'removeItemFromOrder'}">
                <c:url value="controller" var="removeItemFromOrder">
                    <c:param name="command" value="removeItemFromOrder"/>
                    <c:param name="itemId" value="${itemId}"/>
                    <c:param name="itemName" value="${itemName}"/>
                    <c:param name="itemOrderAmount" value="${itemOrderAmount}"/>
                    <c:param name="itemOrderPrice" value="${itemOrderPrice}"/>
                </c:url>
                <form class="remove-item-form" method="post" action="${removeItemFromOrder}">
                    <input type="submit" value="${localeRemove}">
                </form>
            </c:when>
        </c:choose>
    </div>
</div>
<jsp:include page="/WEB-INF/jspf/footer.jspf"/>
</body>
</html>
