<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>
<html>
<head>
    <fmt:message key="page.zReport.title" var="title" scope="page"/>
    <%@ include file="/WEB-INF/jspf/head.jspf" %>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/style/single-block-page-style.css">
</head>
<body>
<jsp:include page="/WEB-INF/jspf/header.jspf"/>

<div class="report-container">
    <table class="report-table">
        <thead>
        <tr>
            <th colspan="4"><fmt:message key="out.report.table.header"/></th>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <td colspan="3"><fmt:message key="ui.table.foot"/></td>
            <td>${xTotalCash}</td>
        </tr>
        </tfoot>
        <tbody>
        <tr>
            <td colspan="3"><fmt:message key="out.report.table.date"/>:</td>
            <td>${xDate}</td>
        </tr>
        <tr>
            <td colspan="3"><fmt:message key="out.report.table.time"/>:</td>
            <td>${xTime}</td>
        </tr>
        <tr>
            <td colspan="3"><fmt:message key="out.report.table.cashier"/>:</td>
            <td>${xUserLogin}</td>
        </tr>
        <tr>
            <td colspan="4"><fmt:message key="out.report.table.type.z"/></td>
        </tr>
        <tr>
            <td colspan="4"><fmt:message key="out.report.table.payments"/>:</td>
        </tr>
        <tr>
            <td colspan="3"><fmt:message key="base.payment.cash"/>:</td>
            <td>${xCash}</td>
        </tr>
        <tr>
            <td colspan="3"><fmt:message key="base.payment.terminal"/>:</td>
            <td>${xTerminal}</td>
        </tr>
        <tr>
            <td colspan="3"><fmt:message key="out.report.table.total.receipts"/>:</td>
            <td>${xTotalReceipts}</td>
        </tr>
        <tr>
            <td colspan="3"><fmt:message key="out.report.table.total.sales"/>:</td>
            <td>${xTotalSales}</td>
        </tr>
        <c:forEach var="saleBean" items="${zSalesList}">
            <tr>
                <td>${saleBean.id}</td>
                <td>${saleBean.date}</td>
                <td>${saleBean.time}</td>
                <td>${saleBean.totalPrice}</td>
            </tr>
        </c:forEach>
        <tr>
            <td colspan="3"><fmt:message key="out.report.table.total.cancellations"/>:</td>
            <td>${xTotalCancels}</td>
        </tr>
        <c:forEach var="cancelBean" items="${zCancelsList}">
            <tr>
                <td>${cancelBean.id}</td>
                <td>${cancelBean.date}</td>
                <td>${cancelBean.time}</td>
                <td>${cancelBean.totalPrice}</td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>

<jsp:include page="/WEB-INF/jspf/footer.jspf"/>
</body>
</html>
