<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>

<html>
<head>
    <fmt:message key="page.receiptList.title" var="title" scope="page"/>
    <%@ include file="/WEB-INF/jspf/head.jspf" %>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/style/list-page-style.css">
</head>
<body class="main-container">
<jsp:include page="/WEB-INF/jspf/header.jspf"/>
<div class="table-container">
    <h3 class="block-title"><fmt:message key="ui.receiptList.table.title"/></h3>
    <hr>
    <c:choose>
        <c:when test="${fn:length(receiptList) == 0}"><fmt:message key="ui.receiptList.emptyListMessage"/></c:when>
        <c:otherwise>
            <div class="table-block">
                <table id="receipt-list">
                    <thead>
                    <tr>
                        <th><fmt:message key="ui.table.header.id"/></th>
                        <th><fmt:message key="ui.receiptList.table.header.date"/></th>
                        <th><fmt:message key="ui.receiptList.table.header.time"/></th>
                        <th><fmt:message key="ui.receiptList.table.header.user"/></th>
                        <th><fmt:message key="ui.receiptList.table.header.total"/></th>
                        <th><fmt:message key="ui.receiptList.table.header.status"/></th>
                        <th><fmt:message key="ui.table.header.select"/></th>
                    </tr>
                    </thead>
                    <tbody>
                    <fmt:message key="ui.table.select" var="localeSubmit"/>
                    <c:forEach var="receipt" items="${receiptList}">
                        <tr class="receipt-row">
                            <td>${receipt.id}</td>
                            <td>${receipt.date}</td>
                            <td>${receipt.time}</td>
                            <td>${receipt.userLogin}</td>
                            <td>${receipt.totalPrice}</td>
                            <td>${receipt.status}</td>
                            <td class="select-row-button">
                                <c:url value="controller" var="showOrder">
                                    <c:param name="command" value="showOrder"/>
                                    <c:param name="receiptId" value="${receipt.id}"/>
                                    <c:param name="totalPrice" value="${receipt.totalPrice}"/>
                                    <c:param name="status" value="${receipt.status}"/>
                                </c:url>
                                <form class="show-order-form" method="post" action="${showOrder}">
                                    <input type="submit" value="${localeSubmit}"
                                           <c:if test="${receipt.status == 'canceled'}">disabled</c:if>/>
                                </form>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
            <div class="pages-block">
                <c:forEach var="pageId" items="${pages}">
                    <c:url value="controller" var="showNextPage">
                        <c:param name="command" value="showAllItems"/>
                        <c:param name="page" value="${pageId}"/>
                    </c:url>
                    <a <c:if test="${page == pageId}">class="active"</c:if>
                       href="${pageContext.request.contextPath}/controller?command=showAllItems&page=${pageId}">
                       ${pageId}
                    </a>
                </c:forEach>
            </div>
        </c:otherwise>
    </c:choose>
</div>
<jsp:include page="/WEB-INF/jspf/footer.jspf"/>
</body>
</html>