<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>
<div class="action-block" id="user-menu-block">
    <h3 class="block-ешеду"><fmt:message key="ui.main.menuBlock.title"/></h3>
    <hr>
    <div class="menu-option" id="show-all-receipts-option">
        <c:url value="controller" var="showAllReceipts">
            <c:param name="command" value="showAllReceipts"/>
        </c:url>
        <form id="show-receipts-form" method="post" action="${showAllReceipts}">
            <button type="submit" class="menu-option-button" id="showReceiptsButton" form="show-receipts-form">
                <fmt:message key="ui.main.menuBlock.receiptList"/>
            </button>
        </form>
    </div>
    <div class="menu-option" id="make-x-report-option">
        <c:url value="controller" var="makeX">
            <c:param name="command" value="makeXReport"/>
        </c:url>
        <form id="make-x-form" method="post" action="${makeX}">
            <button type="submit" class="menu-option-button" id="makeXButton" form="make-x-form"
                    <c:if test="${not empty openedOrder}">disabled</c:if>>
                <fmt:message key="out.report.table.type.x"/>
            </button>
        </form>
    </div>
    <div class="menu-option" id="make-z-report-option">
        <c:url value="controller" var="makeZ">
            <c:param name="command" value="makeZReport"/>
        </c:url>
        <form id="make-z-form" method="post" action="${makeZ}">
            <button type="submit" class="menu-option-button" id="makeZButton" form="make-z-form"
                    <c:if test="${not empty openedOrder}">disabled</c:if>>
                <fmt:message key="out.report.table.type.z"/>
            </button>
        </form>
    </div>
</div>