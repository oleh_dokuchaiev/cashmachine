SET NAMES utf8;

DROP DATABASE IF EXISTS cashDB;
CREATE DATABASE cashDB CHARACTER SET utf8 COLLATE utf8_general_ci;

USE cashDB;
-- --------------------------------------------------------------
-- ROLES
-- users roles
-- --------------------------------------------------------------
CREATE TABLE roles(
	id INTEGER NOT NULL PRIMARY KEY,
	name VARCHAR(45) NOT NULL UNIQUE
);

INSERT INTO roles VALUES(0, 'cashier');
INSERT INTO roles VALUES(1, 'senior-cashier');
INSERT INTO roles VALUES(2, 'commodity-expert');

-- --------------------------------------------------------------
-- LANGUAGES
-- translation languages
-- --------------------------------------------------------------
CREATE TABLE languages(
	id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
	name VARCHAR(45) NOT NULL UNIQUE
);

INSERT INTO languages VALUES(DEFAULT, 'ru');
INSERT INTO languages VALUES(DEFAULT, 'ukr');

-- --------------------------------------------------------------
-- ROLES TRANSLATE
-- users roles translate
-- --------------------------------------------------------------

CREATE TABLE roles_translate(
	id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
	languages_id INTEGER NOT NULL REFERENCES languages(id),
	roles_id INTEGER NOT NULL REFERENCES roles(id),
	name VARCHAR(45) NULL
);

-- roles translation on russian
INSERT INTO roles_translate VALUES(DEFAULT, 1, 0, 'кассир');
INSERT INTO roles_translate VALUES(DEFAULT, 1, 1, 'старший-кассир');
INSERT INTO roles_translate VALUES(DEFAULT, 1, 2, 'товаровед');
-- roles translation on ukrainian
INSERT INTO roles_translate VALUES(DEFAULT, 2, 0, 'касир');
INSERT INTO roles_translate VALUES(DEFAULT, 2, 1, 'старший-касир');
INSERT INTO roles_translate VALUES(DEFAULT, 2, 2, 'товарознавець');

-- --------------------------------------------------------------
-- USERS
-- --------------------------------------------------------------
CREATE TABLE users(
	id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
	login VARCHAR(45) NOT NULL UNIQUE,
	password VARCHAR(45) NOT NULL,
	first_name VARCHAR(45) NOT NULL,
	last_name VARCHAR(45) NOT NULL,
	roles_id INTEGER NOT NULL REFERENCES roles(id)
    ON DELETE CASCADE
    ON UPDATE RESTRICT
);

-- id = 1
INSERT INTO users VALUES(DEFAULT, 'cashier', '111', 'John', 'Smith', 0);
-- id = 2
INSERT INTO users VALUES(DEFAULT, 'senior', '111', 'Иван', 'Петров', 1);
-- id = 3
INSERT INTO users VALUES(DEFAULT, 'commodity', '111', 'Rick', 'Sanchez', 2);

-- --------------------------------------------------------------
-- ITEMS
-- --------------------------------------------------------------
CREATE TABLE items(
	id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
	name VARCHAR(45) NOT NULL UNIQUE,
	amount DECIMAL(19,3) NOT NULL DEFAULT 0.000,
    price DECIMAL(19,2) NOT NULL DEFAULT 0.00
);

-- id = 1
INSERT INTO items VALUES(DEFAULT, 'apples', 50.678, 15.00);
-- id = 2
INSERT INTO items VALUES(DEFAULT, 'loaf', 48, 10.00);
-- id = 3
INSERT INTO items VALUES(DEFAULT, 'milk 500 ml', 36, 12.50);
-- id = 4
INSERT INTO items VALUES(DEFAULT, 'water "Morshynska" still 500 ml', 190, 11.00);
-- id = 5
INSERT INTO items VALUES(DEFAULT, 'drink "Coca-Cola Zero" 500 ml', 150, 13.50);
-- id = 6
INSERT INTO items VALUES(DEFAULT, 'carrot', 100.123, 20.00);
-- id = 7
INSERT INTO items VALUES(DEFAULT, 'banana', 450.780, 13.00);
-- id = 8
INSERT INTO items VALUES(DEFAULT, 'drink "Sprite" 500 ml', 280, 14.30);
-- id = 9
INSERT INTO items VALUES(DEFAULT, 'cake', 60, 72.00);
-- id = 10
INSERT INTO items VALUES(DEFAULT, 'lollipops', 978.450, 31.00);
-- id = 11
INSERT INTO items VALUES(DEFAULT, 'potato', 750, 10.50);
-- id = 12
INSERT INTO items VALUES(DEFAULT, 'tomato', 200.128, 30.00);
-- id = 13
INSERT INTO items VALUES(DEFAULT, 'butter', 500.600, 74.00);
-- id = 14
INSERT INTO items VALUES(DEFAULT, 'pears', 485.300, 19.00);
-- id = 15
INSERT INTO items VALUES(DEFAULT, 'garlic', 436, 14.80);

-- --------------------------------------------------------------
-- ITEMS TRANSLATE
-- --------------------------------------------------------------
CREATE TABLE items_translate(
	id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
	languages_id INTEGER NOT NULL REFERENCES languages(id),
	items_id INTEGER NOT NULL REFERENCES items(id),
	name VARCHAR(45) NULL
);

-- items translation on russian
INSERT INTO items_translate VALUES(DEFAULT, 1, 1, 'яблоки');
INSERT INTO items_translate VALUES(DEFAULT, 1, 2, 'батон');
INSERT INTO items_translate VALUES(DEFAULT, 1, 3, 'молоко 500 мл');
INSERT INTO items_translate VALUES(DEFAULT, 1, 4, 'вода "Моршинская" негазированная 500 мл');
INSERT INTO items_translate VALUES(DEFAULT, 1, 5, 'напиток "Кока-Кола Зеро" 500 мл');
INSERT INTO items_translate VALUES(DEFAULT, 1, 6, 'морковь');
INSERT INTO items_translate VALUES(DEFAULT, 1, 7, 'бананы');
INSERT INTO items_translate VALUES(DEFAULT, 1, 8, 'напиток "Спрайт" 500 мл');
INSERT INTO items_translate VALUES(DEFAULT, 1, 9, 'торт');
INSERT INTO items_translate VALUES(DEFAULT, 1, 10, 'леденцы');
INSERT INTO items_translate VALUES(DEFAULT, 1, 11, 'картофель');
INSERT INTO items_translate VALUES(DEFAULT, 1, 12, 'помидоры');
INSERT INTO items_translate VALUES(DEFAULT, 1, 13, 'масло');
INSERT INTO items_translate VALUES(DEFAULT, 1, 14, 'груши');
INSERT INTO items_translate VALUES(DEFAULT, 1, 15, 'чеснок');

-- items translation on ukrainian
INSERT INTO items_translate VALUES(DEFAULT, 2, 1, 'яблука');
INSERT INTO items_translate VALUES(DEFAULT, 2, 2, 'батон');
INSERT INTO items_translate VALUES(DEFAULT, 2, 3, 'молоко 500 мл');
INSERT INTO items_translate VALUES(DEFAULT, 2, 4, 'вода "Моршинська" негазована 500 мл');
INSERT INTO items_translate VALUES(DEFAULT, 2, 5, 'напій "Кока-Кола Зеро" 500 мл');
INSERT INTO items_translate VALUES(DEFAULT, 2, 6, 'морква');
INSERT INTO items_translate VALUES(DEFAULT, 2, 7, 'банани');
INSERT INTO items_translate VALUES(DEFAULT, 2, 8, 'напій "Спрайт" 500 мл');
INSERT INTO items_translate VALUES(DEFAULT, 2, 9, 'торт');
INSERT INTO items_translate VALUES(DEFAULT, 2, 10, 'льодяники');
INSERT INTO items_translate VALUES(DEFAULT, 2, 11, 'картопля');
INSERT INTO items_translate VALUES(DEFAULT, 2, 12, 'томати');
INSERT INTO items_translate VALUES(DEFAULT, 2, 13, 'масло');
INSERT INTO items_translate VALUES(DEFAULT, 2, 14, 'груші');
INSERT INTO items_translate VALUES(DEFAULT, 2, 15, 'часник');

-- --------------------------------------------------------------
-- STATUSES
-- --------------------------------------------------------------
CREATE TABLE statuses(
	id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
	name VARCHAR(45) NOT NULL UNIQUE
);

-- id = 1
INSERT INTO statuses VALUES(DEFAULT, 'opened');
-- id = 2
INSERT INTO statuses VALUES(DEFAULT, 'paid');
-- id = 3
INSERT INTO statuses VALUES(DEFAULT, 'canceled');

-- --------------------------------------------------------------
-- STATUSES TRANSLATE
-- --------------------------------------------------------------
CREATE TABLE statuses_translate(
    id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    languages_id INTEGER NOT NULL REFERENCES languages(id),
    statuses_id INTEGER NOT NULL REFERENCES statuses(id),
    name VARCHAR(45) NULL
);

-- statuses translation on russian
INSERT INTO statuses_translate VALUES (DEFAULT, 1, 1, 'открыт');
INSERT INTO statuses_translate VALUES (DEFAULT, 1, 2, 'оплачен');
INSERT INTO statuses_translate VALUES (DEFAULT, 1, 3, 'отменен');

-- statuses translation on ukrainian
INSERT INTO statuses_translate VALUES (DEFAULT, 2, 1, 'відкрите');
INSERT INTO statuses_translate VALUES (DEFAULT, 2, 2, 'сплачене');
INSERT INTO statuses_translate VALUES (DEFAULT, 2, 3, 'відмінене');

-- --------------------------------------------------------------
-- PAYMENTS
-- --------------------------------------------------------------
CREATE TABLE payments(
    id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(45) NOT NULL UNIQUE
);

-- id = 1
INSERT INTO payments VALUES(DEFAULT, 'cash');
-- id = 2
INSERT INTO payments VALUES(DEFAULT, 'terminal');

-- --------------------------------------------------------------
-- PAYMENTS TRANSLATE
-- --------------------------------------------------------------
CREATE TABLE payments_translate(
    id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    languages_id INTEGER NOT NULL REFERENCES languages(id),
    payments_id INTEGER NOT NULL REFERENCES payments(id),
    name VARCHAR(45) NULL
);

-- payments translation on russian
INSERT INTO payments_translate VALUES (DEFAULT, 1, 1, 'наличный');
INSERT INTO payments_translate VALUES (DEFAULT, 1, 2, 'безналичный');

-- payments translation on ukrainian
INSERT INTO payments_translate VALUES (DEFAULT, 2, 1, 'готівка');
INSERT INTO payments_translate VALUES (DEFAULT, 2, 2, 'термінал');

-- --------------------------------------------------------------
-- RECEIPTS
-- --------------------------------------------------------------
CREATE TABLE receipts(
	id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
	datetime TIMESTAMP NOT NULL,
	total_price DECIMAL(19,2) NOT NULL DEFAULT 0.00,
	users_id INTEGER NOT NULL REFERENCES users(id),
	statuses_id INTEGER NOT NULL REFERENCES statuses(id),
	payments_id INTEGER NOT NULL REFERENCES payments(id)
);

-- --------------------------------------------------------------
-- ORDERS
-- --------------------------------------------------------------
CREATE TABLE orders(
	receipts_id INTEGER NOT NULL REFERENCES receipts(id)
    ON DELETE CASCADE,
	items_id INTEGER NOT NULL REFERENCES items(id),
	item_amount DECIMAL(19,3) NOT NULL DEFAULT 0.000,
	item_price DECIMAL(19,2) NOT NULL DEFAULT 0.00
);

-- --------------------------------------------------------------
-- TEST DB
-- --------------------------------------------------------------
SELECT * FROM roles;
SELECT * FROM languages;
SELECT * FROM roles_translate;
SELECT * FROM users;
SELECT * FROM items;
SELECT * FROM items_translate;
SELECT * FROM statuses;