package com.epam.dokuchaiev.java.cashMachine.test;

import com.epam.dokuchaiev.java.cashMachine.db.DBConnectionPool;

import com.epam.dokuchaiev.java.cashMachine.exceptions.DBException;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.Assert;
import org.mockito.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class TestAddItemToOrderCommand {
    private static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
    private static final String DB_URL = "jdbc:h2:~/p8db";
    private static final String URL_CONNECTION = "jdbc:mysql:~/cashdb?allowPublicKeyRetrieval=true&amp;useSSL=false&amp;useUnicode=true&amp;useJDBCCompliantTimezoneShift=true&amp;useLegacyDatetimeCode=false&amp;serverTimezone=UTC";
    private static final String USER = "root";
    private static final String PASS = "root";

    @Spy
    private static DBConnectionPool connectionPool;

    @BeforeClass
    public static void beforeTest() throws ClassNotFoundException, DBException {
        Class.forName(JDBC_DRIVER);
        connectionPool = DBConnectionPool.getInstance();
    }

    
}
