package com.epam.dokuchaiev.java.cashMachine.web.command;

import com.epam.dokuchaiev.java.cashMachine.Path;
import com.epam.dokuchaiev.java.cashMachine.db.Role;
import com.epam.dokuchaiev.java.cashMachine.db.bean.UserBean;
import com.epam.dokuchaiev.java.cashMachine.db.dao.OrderDAO;
import com.epam.dokuchaiev.java.cashMachine.db.dao.UserDAO;
import com.epam.dokuchaiev.java.cashMachine.exceptions.AppException;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class OpenMainCommand extends Command {
    private static final Logger LOGGER = Logger.getLogger(OpenMainCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, AppException {
        LOGGER.debug("Command starts");

        HttpSession session = request.getSession(false);
        UserBean userBean = (UserBean) session.getAttribute("user");

        String localeTag = String.valueOf(session.getAttribute("locale"));
        LOGGER.trace("Get session attribute: locale --> " + localeTag);
        Role role = (Role) session.getAttribute("userRole");
        String localeRole = new UserDAO().getRole(role, localeTag);
        userBean.setRole(localeRole);
        session.setAttribute("localeRole", localeRole);
        LOGGER.trace("Set session attribute for i18n: localeRole --> " + localeRole);

        int openedOrder = OrderDAO.checkOpenedOrders();
        if (openedOrder != 0) {
            session.setAttribute("openedOrder", openedOrder);
            LOGGER.trace("Set the session attribute: openedOrder --> " + openedOrder);
        } else {
            LOGGER.trace("No opened orders found");
        }

        String forward = (request.getSession(false).getAttribute("user") != null) ? Path.PAGE_HOME : Path.PAGE_INDEX;
        LOGGER.trace("Set forward: " + forward);

        LOGGER.debug("Command finished");
        return forward;
    }
}
