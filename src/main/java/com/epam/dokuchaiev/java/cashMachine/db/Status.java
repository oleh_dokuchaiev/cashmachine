package com.epam.dokuchaiev.java.cashMachine.db;

public enum Status {
    OPENED, PAID, CANCELED;

    public String getName() {
        return name().toLowerCase();
    }
}
