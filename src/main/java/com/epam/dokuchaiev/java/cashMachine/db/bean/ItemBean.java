package com.epam.dokuchaiev.java.cashMachine.db.bean;

import com.epam.dokuchaiev.java.cashMachine.db.entity.Entity;

import java.util.Locale;

public class ItemBean extends Entity {
    private String name;
    private String price;
    private String amount;
    private String orderPrice;
    private String orderAmount;

    public String getOrderPrice() {
        return orderPrice;
    }

    public void setOrderPrice(String orderPrice) {
        this.orderPrice = orderPrice;
    }

    public String getOrderAmount() {
        return orderAmount;
    }

    public void setOrderAmount(String orderAmount) {
        this.orderAmount = orderAmount;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = String.format(Locale.US, "%.2f", price);
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = String.format(Locale.US, "%.3f", amount);
    }

    public String getName() {
        return name;
    }

    public void setName(String localizedName) {
        this.name = localizedName;
    }

    @Override
    public String toString() {
        return "Item ["+getId()+"] " + name + ": amount(" + amount + "), price(" + price+ ")";
    }

//    @Override
//    public ItemBean getBean(Item item) {
//        ItemBean itemBean = new ItemBean();
//        itemBean.setId(item.getId());
//        itemBean.setName(item.getName());
//        itemBean.setAmount(item.getAmount());
//        itemBean.setPrice(item.getPrice());
//        return itemBean;
//    }
}
