package com.epam.dokuchaiev.java.cashMachine.web.filter;

import com.epam.dokuchaiev.java.cashMachine.Path;
import com.epam.dokuchaiev.java.cashMachine.db.Role;
import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.*;

public class CommandAccessFilter implements Filter {
    private static final Logger LOGGER = Logger.getLogger(CommandAccessFilter.class);

    private final Map<Role, List<String>> accessMap = new HashMap<>();
    private List<String> commons = new ArrayList<>();
    private List<String> outOfControl = new ArrayList<>();

    @Override
    public void init(FilterConfig filterConfig) {
        LOGGER.debug("Filter initialization starts");

        accessMap.put(Role.CASHIER, asList(filterConfig.getInitParameter("cashier")));
        accessMap.put(Role.SENIOR_CASHIER, asList(filterConfig.getInitParameter("senior_cashier")));
        accessMap.put(Role.COMMODITY_EXPERT, asList(filterConfig.getInitParameter("commodity_expert")));
        LOGGER.trace("Access Map --> " + accessMap);

        commons = asList(filterConfig.getInitParameter("common"));
        LOGGER.trace("Common commands --> " + commons);

        outOfControl = asList(filterConfig.getInitParameter("out-of-control"));
        LOGGER.trace("Out of control commands --> " + outOfControl);

        LOGGER.debug("Filter initialization finished");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        LOGGER.debug("Filter starts");

        if (accessAllowed(request)) {
            LOGGER.debug("Filter finished");
            chain.doFilter(request, response);
        } else {
            String ERROR_MESSAGE = "You don't have permission to access the requested resource";

            request.setAttribute("errorMessage", ERROR_MESSAGE);
            LOGGER.trace("Request attribute set: errorMessage --> " + ERROR_MESSAGE);

            request.getRequestDispatcher(Path.PAGE_ERROR_PAGE).forward(request, response);
        }
    }

    private boolean accessAllowed(ServletRequest request) {
        HttpServletRequest httpRequest = (HttpServletRequest) request;

        String commandName = request.getParameter("command");
        LOGGER.trace("Command Name --> " + commandName);
        if (commandName == null || commandName.isEmpty()) {
            return false;
        }

        if (outOfControl.contains(commandName)) {
            return true;
        }

        HttpSession session = httpRequest.getSession(false);
        if (session == null) {
            return false;
        }

        Role userRole = (Role)session.getAttribute("userRole");
        LOGGER.trace("User role --> " + userRole);
        if (userRole == null) {
            return false;
        }

        return accessMap.get(userRole).contains(commandName) || commons.contains(commandName);
    }

    @Override
    public void destroy() {
        LOGGER.debug("Filter destruction starts");

        LOGGER.debug("Filter destruction finished");
    }

    private List<String> asList(String string) {
        List<String> list = new ArrayList<>();
        StringTokenizer st = new StringTokenizer(string);
        while (st.hasMoreTokens()) {
            list.add(st.nextToken());
        }
        return list;
    }
}
