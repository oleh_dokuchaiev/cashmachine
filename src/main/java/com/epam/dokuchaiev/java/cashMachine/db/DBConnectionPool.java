package com.epam.dokuchaiev.java.cashMachine.db;

import com.epam.dokuchaiev.java.cashMachine.exceptions.DBException;
import com.epam.dokuchaiev.java.cashMachine.exceptions.Messages;
import org.apache.log4j.Logger;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class DBConnectionPool {
    private static final Logger LOGGER = Logger.getLogger(DBConnectionPool.class);

    private static List<Connection> connectionPool;
    private static final List<Connection> busyConnections = new ArrayList<>();
    private static final int INITIAL_POOL_SIZE = 10;
    private static final int MAX_POOL_SIZE = 30;
    private static final int MAX_TIMEOUT = 10000;

    private static DataSource dataSource;
    private static DBConnectionPool dbConnectionPool;

    public static synchronized DBConnectionPool getInstance() throws DBException {
        if (dbConnectionPool == null) {
            List<Connection> connectionPool = new ArrayList<>(INITIAL_POOL_SIZE);
            dbConnectionPool = new DBConnectionPool(connectionPool);
            for (int i = 0; i < INITIAL_POOL_SIZE; i++) {
                connectionPool.add(createConnection());
            }
        }
        return dbConnectionPool;
    }

    private DBConnectionPool(List<Connection> connectionPool) throws DBException {
        DBConnectionPool.connectionPool = connectionPool;
        try {
            Context initContext = new InitialContext();
            Context envContext = (Context) initContext.lookup("java:/comp/env");
            dataSource = (DataSource) envContext.lookup("jdbc/cashdb");
        } catch (NamingException namingException) {
            LOGGER.error(Messages.ERR_CANNOT_OBTAIN_DATA_SOURCE);
            throw new DBException(Messages.ERR_CANNOT_OBTAIN_DATA_SOURCE, namingException);
        }
    }

    private static Connection createConnection() throws DBException {
        Connection connection;
        try {
            connection = dataSource.getConnection();
            LOGGER.trace("Create connection --> " + connection);
        } catch (SQLException sqlException) {
            LOGGER.error(Messages.ERR_CANNOT_CREATE_CONNECTION);
            LOGGER.error(sqlException.getMessage());
            throw new DBException(Messages.ERR_CANNOT_CREATE_CONNECTION, sqlException);
        }
        return connection;
    }

    public Connection getConnection() throws DBException {
        if (connectionPool.isEmpty()) {
            if (busyConnections.size() < MAX_POOL_SIZE) {
                connectionPool.add(createConnection());
            } else {
                throw new RuntimeException(Messages.ERR_NO_AVAILABLE_CONNECTIONS);
            }
        }
        Connection connection = connectionPool.remove(connectionPool.size() - 1);
        try {
            if (!connection.isValid(MAX_TIMEOUT)) {
                connection = createConnection();
            }
        } catch (SQLException sqlException) {
            LOGGER.error(Messages.ERR_CANNOT_OBTAIN_CONNECTION);
            throw new DBException(Messages.ERR_CANNOT_OBTAIN_CONNECTION, sqlException);
        }
        busyConnections.add(connection);
        return connection;
    }

    public void releaseConnection(Connection connection) {
        connectionPool.add(connection);
        busyConnections.remove(connection);
    }

    public void rollback(Connection connection) throws DBException {
        if (connection != null) {
            try {
                connection.rollback();
            } catch (SQLException sqlException) {
                LOGGER.error(Messages.ERR_CANNOT_ROLLBACK_TRANSACTION);
                throw new DBException(Messages.ERR_CANNOT_ROLLBACK_TRANSACTION, sqlException);
            }
        }
    }

    public void closeResources(ResultSet resultSet, Statement statement) throws DBException {
        close(resultSet);
        close(statement);
    }

    public void close(Statement statement) throws DBException {
        if (statement != null) {
            try {
                statement.close();
            } catch (SQLException sqlException) {
                LOGGER.error(Messages.ERR_CANNOT_CLOSE_STATEMENT);
                throw new DBException(Messages.ERR_CANNOT_CLOSE_STATEMENT, sqlException);
            }
        }
    }

    public void close(ResultSet resultSet) throws DBException {
        if (resultSet != null) {
            try {
                resultSet.close();
            } catch (SQLException sqlException) {
                LOGGER.error(Messages.ERR_CANNOT_CLOSE_RESULT_SET);
                throw new DBException(Messages.ERR_CANNOT_CLOSE_RESULT_SET, sqlException);
            }
        }
    }
}
