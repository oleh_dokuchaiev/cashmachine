package com.epam.dokuchaiev.java.cashMachine.db.entity;

import com.epam.dokuchaiev.java.cashMachine.db.Role;

public class User extends Entity {
    private Role role;
    private String login;
    private String firstName;
    private String lastName;
    private String password;

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public String toString() {
        return "User [login: " + login + ", role: " + role.getName() + "] firstName: " + firstName + ", lastName: " + lastName;
    }
}
