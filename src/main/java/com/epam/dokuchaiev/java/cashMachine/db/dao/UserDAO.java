package com.epam.dokuchaiev.java.cashMachine.db.dao;

import com.epam.dokuchaiev.java.cashMachine.db.DBConnectionPool;
import com.epam.dokuchaiev.java.cashMachine.db.Fields;
import com.epam.dokuchaiev.java.cashMachine.db.Role;
import com.epam.dokuchaiev.java.cashMachine.db.entity.User;
import com.epam.dokuchaiev.java.cashMachine.exceptions.DBException;
import com.epam.dokuchaiev.java.cashMachine.exceptions.Messages;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class UserDAO extends DAO {
    private static final Logger LOGGER = Logger.getLogger(UserDAO.class);
    private static final Lock LOCK = new ReentrantLock();

    private static final String SQL_FIND_USER_BY_LOGIN = "SELECT * FROM users WHERE login=?";
    private static final String SQL_FIND_USER_BY_ID = "SELECT * FROM users WHERE id=?";
    private static final String SQL_GET_DEFAULT_ROLE_NAME = "SELECT name FROM roles WHERE id=?";
    private static final String SQL_GET_TRANSLATED_ROLE_NAME = "SELECT name FROM roles_translate WHERE roles_id=? AND languages_id=?";
//    private static final String SQL_GET_LANGUAGE_ID = "SELECT id FROM languages WHERE name=?";

//    private static final DBConnectionPool connectionPool = DBConnectionPool.getInstance();
    private static final DBConnectionPool connectionPool = initConnectionPool();

    private static DBConnectionPool initConnectionPool() {
        DBConnectionPool instance = null;
        try {
            instance = DBConnectionPool.getInstance();
        } catch (DBException dbException) {
            LOGGER.error(Messages.ERR_CANNOT_GET_CONNECTION_POOL_INSTANCE, dbException);
        }
        return instance;
    }

    @Override
    public User findById(int id) throws DBException {
        User user = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Connection connection = connectionPool.getConnection();

        try {
            preparedStatement = connection.prepareStatement(SQL_FIND_USER_BY_ID);
            preparedStatement.setString(1, String.valueOf(id));
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                user = getUser(resultSet);
            }
            connection.commit();
        } catch (SQLException sqlException) {
            connectionPool.rollback(connection);
            throw new DBException(Messages.ERR_CANNOT_OBTAIN_USER_BY_ID, sqlException);
        } finally {
            connectionPool.closeResources(resultSet, preparedStatement);
            connectionPool.releaseConnection(connection);
        }
        return user;
    }

    public User findByName(String login) throws DBException {
        User user = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Connection connection = connectionPool.getConnection();

        try {
            preparedStatement = connection.prepareStatement(SQL_FIND_USER_BY_LOGIN);
            preparedStatement.setString(1, login);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                user = getUser(resultSet);
            }
            connection.commit();
        } catch (SQLException sqlException) {
            connectionPool.rollback(connection);
            throw new DBException(Messages.ERR_CANNOT_OBTAIN_USER_BY_LOGIN, sqlException);
        } finally {
            connectionPool.closeResources(resultSet, preparedStatement);
            connectionPool.releaseConnection(connection);
        }
        return user;
    }

//    @Override
//    public List<User> getEntities() throws DBException {
//        return null;
//    }

    public String getRole(Role role, String localeTag) throws DBException {
        String roleName;
        int roleId = role.ordinal();
        LOGGER.debug("Get role id: roleId --> " + roleId);
        int languageId = DAO.getLanguageId(localeTag);
        String sqlStatement = (languageId != 0) ? SQL_GET_TRANSLATED_ROLE_NAME : SQL_GET_DEFAULT_ROLE_NAME;
        LOGGER.debug("Get language id: languageId --> " + languageId);
//        if (languageId != 0) {
//        ResultSet resultSet = null;
//        PreparedStatement preparedStatement = null;
        Connection connection = connectionPool.getConnection();
        try {
            LOCK.lock();
            roleName = formatName(DAO.getName(connection, sqlStatement, roleId, languageId));
            LOGGER.debug("Get role name: roleName --> " + roleName);
            connection.commit();
        } catch (SQLException sqlException) {
            connectionPool.rollback(connection);
            LOGGER.error(Messages.ERR_CANNOT_OBTAIN_ROLE_NAME);
            throw new DBException(Messages.ERR_CANNOT_OBTAIN_ROLE_NAME, sqlException);
        } finally {
            LOCK.unlock();
            connectionPool.releaseConnection(connection);
        }
//        try {
//            LOCK.lock();
//            preparedStatement = connection.prepareStatement(sqlStatement);
//            preparedStatement.setInt(1, roleId);
//            if (languageId != 0) {
//                preparedStatement.setInt(2, languageId);
//            }
//            resultSet = preparedStatement.executeQuery();
//            if (resultSet.next()) {
//                roleName = formatName(resultSet.getString(Fields.ENTITY_NAME));
//            }
//            connection.commit();
//        } catch (SQLException sqlException) {
//            connectionPool.rollback(connection);
//            LOGGER.error(Messages.ERR_CANNOT_OBTAIN_TRANSLATED_ROLE_NAME, sqlException);
//            throw new DBException(Messages.ERR_CANNOT_OBTAIN_TRANSLATED_ROLE_NAME, sqlException);
//        } finally {
//            connectionPool.closeResources(resultSet, preparedStatement);
//            LOCK.unlock();
//            connectionPool.releaseConnection(connection);
//        }
//        } else {
//            roleName = formatName(role.getName());
//        }
        return roleName;
    }

    @Override
    public String formatName(String name) {
        StringBuilder result = new StringBuilder();
        String[] splitName = name.split("[-\\s_]");
        for (String split : splitName) {
            split = Character.toUpperCase(split.charAt(0)) + split.substring(1).toLowerCase();
            result.append(split).append(" ");
        }
        return result.toString().trim();
    }

//    @Override
//    public int getLanguageId(String localeTag) throws DBException {
//        int languageId = 0;
//        PreparedStatement preparedStatement = null;
//        ResultSet resultSet = null;
//        Connection connection = connectionPool.getConnection();
//        try {
//            preparedStatement = connection.prepareStatement(SQL_GET_LANGUAGE_ID);
//            preparedStatement.setString(1, localeTag);
//            resultSet = preparedStatement.executeQuery();
//            if (resultSet.next()) {
//                languageId = resultSet.getInt(Fields.ENTITY_ID);
//            }
//            connection.commit();
//        } catch (SQLException sqlException) {
//            LOGGER.error(Messages.ERR_CANNOT_OBTAIN_LANGUAGE_ID);
//            throw new DBException(Messages.ERR_CANNOT_OBTAIN_LANGUAGE_ID, sqlException);
//        }
//        return languageId;
//    }

    private static User getUser(ResultSet resultSet) throws SQLException {
        User user = new User();
        user.setId(resultSet.getInt(Fields.ENTITY_ID));
        user.setLogin(resultSet.getString(Fields.USER_LOGIN));
        user.setPassword(resultSet.getString(Fields.USER_PASSWORD));
        user.setFirstName(resultSet.getString(Fields.USER_FIRST_NAME));
        user.setLastName(resultSet.getString(Fields.USER_LAST_NAME));
        user.setRole(Role.values()[resultSet.getInt(Fields.USER_ROLE_ID)]);
        return user;
    }
}
