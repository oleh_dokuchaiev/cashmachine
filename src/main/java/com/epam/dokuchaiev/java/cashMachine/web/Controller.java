package com.epam.dokuchaiev.java.cashMachine.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.epam.dokuchaiev.java.cashMachine.Path;
import com.epam.dokuchaiev.java.cashMachine.exceptions.AppException;
import com.epam.dokuchaiev.java.cashMachine.web.command.Command;
import com.epam.dokuchaiev.java.cashMachine.web.command.CommandContainer;

public class Controller extends HttpServlet {
    private static final Logger LOGGER = Logger.getLogger(Controller.class);

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        process(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        process(request, response);
    }

    private void process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LOGGER.debug("Controller starts");

        String commandName = request.getParameter("command");
        LOGGER.trace("Request parameter: command --> " + commandName);

        Command command = CommandContainer.get(commandName);
        LOGGER.trace("Obtained command --> " + command);

        String forward = Path.PAGE_ERROR_PAGE;
        try {
            forward = command.execute(request, response);
        } catch (AppException | IOException | ServletException appException) {
            request.setAttribute("errorMessage", appException.getMessage());
        }
        LOGGER.trace("Forward adress --> " + forward);
        LOGGER.debug("Controller finished. Proceed to forward adress --> " + forward);

        request.getRequestDispatcher(forward).forward(request, response);
    }
}
