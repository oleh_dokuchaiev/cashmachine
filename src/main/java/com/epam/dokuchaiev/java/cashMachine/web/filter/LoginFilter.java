package com.epam.dokuchaiev.java.cashMachine.web.filter;

import com.epam.dokuchaiev.java.cashMachine.Path;
import org.apache.log4j.Logger;

import javax.servlet.*;
import java.io.IOException;
import java.util.HashSet;

public class LoginFilter implements Filter {
    private static final Logger LOGGER =Logger.getLogger(LoginFilter.class);

    @Override
    public void init(FilterConfig filterConfig) {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

            LOGGER.debug("Filter starts");
            ServletContext context = request.getServletContext();
            String login = request.getParameter("login");
            if (isLoggedIn(context, login)) {
                String errorMessage = "This user already logged in";
                request.setAttribute("errorMessage", errorMessage);
                LOGGER.trace("Set request attribute: errorMessage --> " + errorMessage);
                request.getRequestDispatcher(Path.PAGE_ERROR_PAGE).forward(request, response);
            } else {
                LOGGER.debug("Filter finished");
                chain.doFilter(request, response);
            }
    }

    private static boolean isLoggedIn(ServletContext context, String login) {
        HashSet<String> loggedInUsers = (HashSet<String>) context.getAttribute("loggedInUsers");
        if (loggedInUsers != null) {
            LOGGER.debug("Get context attribute: loggedInUsers --> " + loggedInUsers.toString());
            boolean isLogged = loggedInUsers.contains(login);
            LOGGER.debug("User logged: " + isLogged);
            return isLogged;
        } else {
            return false;
        }
    }

    @Override
    public void destroy() {
    }
}
