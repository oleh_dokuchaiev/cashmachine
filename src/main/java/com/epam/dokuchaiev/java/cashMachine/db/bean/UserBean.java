package com.epam.dokuchaiev.java.cashMachine.db.bean;

import com.epam.dokuchaiev.java.cashMachine.db.entity.Entity;

public class UserBean extends Entity {
    private String role;
    private String login;
    private String firstName;
    private String lastName;

    public void setRole(String role) {
        this.role = role;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getRole() {
        return role;
    }

    public String getLastName() {
        return lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLogin() {
        return login;
    }

    @Override
    public String toString() {
        return "User: " + login + " [" + role + "]";
    }
}
