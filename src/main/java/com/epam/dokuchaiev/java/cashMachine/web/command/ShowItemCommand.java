package com.epam.dokuchaiev.java.cashMachine.web.command;

import com.epam.dokuchaiev.java.cashMachine.Path;
import com.epam.dokuchaiev.java.cashMachine.db.Role;
import com.epam.dokuchaiev.java.cashMachine.db.bean.ItemBean;
import com.epam.dokuchaiev.java.cashMachine.db.dao.ItemDAO;
import com.epam.dokuchaiev.java.cashMachine.db.dao.OrderDAO;
import com.epam.dokuchaiev.java.cashMachine.exceptions.AppException;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Map;

public class ShowItemCommand extends Command {
    private static final Logger LOGGER = Logger.getLogger(ShowItemCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, AppException {
        LOGGER.debug("Command starts");

        HttpSession session = request.getSession(false);
        int itemId = Integer.parseInt(request.getParameter("itemId"));
        ItemDAO itemDAO = new ItemDAO();
        String localeTag = String.valueOf(session.getAttribute("locale"));
        LOGGER.trace("Get session attribute: locale --> " + localeTag);
        ItemBean itemBean = itemDAO.getItem(itemId, localeTag);
        request.setAttribute("item", itemBean);
        LOGGER.trace("Set request attribute: item --> " + itemBean);

        String source = request.getParameter("source");
        Role userRole = Role.valueOf(String.valueOf(session.getAttribute("userRole")));

        String commandName = getCommandName(source, userRole);
        LOGGER.trace("Get request parameter: commandName --> " + commandName);

        switch (commandName) {
            case ("addItemToOrder"): {
                Map<Integer, Double> orderItemsMap = OrderDAO.getOrderItemsMap(Integer.parseInt(String.valueOf(session.getAttribute("openedOrder"))));
                if (orderItemsMap.containsKey(itemId)) {
                    LOGGER.trace("Found item[" + itemId + "] in order[" + session.getAttribute("openedOrder") + "]. Proceed to command: " + "changeItemAmountInOrder");
                    double itemOrderAmount = orderItemsMap.get(itemId);
                    double itemOrderPrice = new OrderDAO().calcPrice(itemId, itemOrderAmount);
                    request.setAttribute("itemOrderAmount", itemOrderAmount);
                    LOGGER.trace("Set request attribute: itemOrderAmount --> " + itemOrderAmount);
                    request.setAttribute("itemOrderPrice", itemOrderPrice);
                    LOGGER.trace("Set request attribute: itemOrderPrice --> " + itemOrderPrice);
                    request.setAttribute("commandName", "changeItemAmountInOrder");
                    LOGGER.trace("Set request attribute: commandName --> " + "changeItemAmountInOrder");
                } else {
                    request.setAttribute("commandName", commandName);
                    LOGGER.trace("Set request attribute: commandName --> " + commandName);
                }
                break;
            }
            case ("changeItemAmountInStorage"): {
                request.setAttribute("commandName", commandName);
                LOGGER.trace("Set request attribute: commandName --> " + commandName);
                break;
            }
            case ("changeItemAmountInOrder"):
            case ("removeItemFromOrder"): {
                double itemOrderAmount = Double.parseDouble(request.getParameter("itemOrderAmount"));
                double itemOrderPrice = Double.parseDouble(request.getParameter("itemOrderPrice"));
                request.setAttribute("itemOrderAmount", itemOrderAmount);
                LOGGER.trace("Set request attribute: itemOrderAmount --> " + itemOrderAmount);
                request.setAttribute("itemOrderPrice", itemOrderPrice);
                LOGGER.trace("Set request attribute: itemOrderPrice --> " + itemOrderPrice);
                request.setAttribute("commandName", commandName);
                LOGGER.trace("Set request attribute: commandName --> " + commandName);
                break;
            }
            default: {
                return Path.PAGE_ERROR_PAGE;
            }
        }

        LOGGER.debug("Command finished");
        return Path.PAGE_ITEM;
    }

    private String getCommandName(String source, Role userRole) {
        return (userRole.equals(Role.CASHIER)) ?
                (source.equals("order") ? "changeItemAmountInOrder" : "addItemToOrder") :
                (source.equals("order") ? "removeItemFromOrder" : "changeItemAmountInStorage");
    }
}
