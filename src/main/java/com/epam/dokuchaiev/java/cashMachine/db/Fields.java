package com.epam.dokuchaiev.java.cashMachine.db;

public final class Fields {
    public static final String ENTITY_ID = "id";
    public static final String ENTITY_NAME = "name";

    /**
     * User field names
     */
    public static final String USER_LOGIN = "login";
    public static final String USER_PASSWORD = "password";
    public static final String USER_FIRST_NAME = "first_name";
    public static final String USER_LAST_NAME = "last_name";
    public static final String USER_ROLE_ID = "roles_id";

    /**
     * Item field names
     */
    public static final String ITEM_PRICE = "price";
    public static final String ITEM_AMOUNT = "amount";

    /**
     * Receipt field names
     */
    public static final String RECEIPT_DATETIME = "datetime";
    public static final String RECEIPT_TOTAL_PRICE = "total_price";
    public static final String RECEIPT_USERS_ID = "users_id";
    public static final String RECEIPT_STATUS_ID = "statuses_id";
    public static final String RECEIPT_PAYMENT_ID = "payments_id";

    /**
     * Order field names
     */
    public static final String ORDER_ITEMS_ID = "items_id";
    public static final String ORDER_ITEM_AMOUNT = "item_amount";
    public static final String ORDER_ITEM_PRICE = "item_price";
}
