package com.epam.dokuchaiev.java.cashMachine.db.dao;

import com.epam.dokuchaiev.java.cashMachine.db.DBConnectionPool;
import com.epam.dokuchaiev.java.cashMachine.db.Fields;
import com.epam.dokuchaiev.java.cashMachine.db.entity.Entity;
import com.epam.dokuchaiev.java.cashMachine.exceptions.DBException;
import com.epam.dokuchaiev.java.cashMachine.exceptions.Messages;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public abstract class DAO {
    private static final Logger LOGGER = Logger.getLogger(DAO.class);
    private static final DBConnectionPool connectionPool = initConnectionPool();

    private static DBConnectionPool initConnectionPool() {
        DBConnectionPool instance = null;
        try {
            instance = DBConnectionPool.getInstance();
        } catch (DBException dbException) {
            LOGGER.error(Messages.ERR_CANNOT_GET_CONNECTION_POOL_INSTANCE, dbException);
        }
        return instance;
    }

    public abstract Entity findById(int id) throws DBException;

    /**
     * Method gets language id from database by provided locale language tag
     * @param localeTag
     * @return
     * @throws DBException
     */
    static int getLanguageId(String localeTag) throws DBException {
        int languageId = 0;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Connection connection = connectionPool.getConnection();
        try {
            preparedStatement = connection.prepareStatement("SELECT id FROM languages WHERE name=?");
            preparedStatement.setString(1, localeTag);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                languageId = resultSet.getInt(Fields.ENTITY_ID);
            }
            connection.commit();
        } catch (SQLException sqlException) {
            connectionPool.rollback(connection);
            throw new DBException(Messages.ERR_CANNOT_OBTAIN_LANGUAGE_ID, sqlException);
        } finally {
            connectionPool.closeResources(resultSet, preparedStatement);
            connectionPool.releaseConnection(connection);
        }
        return languageId;
    }

    /**
     * Method gets name of entity from database. Name depends on provided locale language tag and provided from specific DAO method sql statement
     * @param connection
     * @param sqlStatement
     * @param id
     * @param languageId
     * @return
     * @throws SQLException
     * @throws DBException
     */
    static String getName(Connection connection, String sqlStatement, int id, int languageId) throws SQLException, DBException {
        LOGGER.debug("SQL parameters: id --> " + id + "; languageId --> " + languageId + "; sqlStatement --> " + sqlStatement);
        String name = null;
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(sqlStatement);
            preparedStatement.setInt(1, id);
            if (languageId != 0) {
                preparedStatement.setInt(2, languageId);
            }
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                name = resultSet.getString(Fields.ENTITY_NAME);
            }
        } finally {
            connectionPool.closeResources(resultSet, preparedStatement);
        }
        return name;
    }

    /**
     * Absttract class formats name to specific standard for each specific entity. Realization depends on which entity data is processing on
     * @param name
     * @return
     */
    abstract String formatName(String name);
}
