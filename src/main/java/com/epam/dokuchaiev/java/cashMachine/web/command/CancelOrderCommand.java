package com.epam.dokuchaiev.java.cashMachine.web.command;

import com.epam.dokuchaiev.java.cashMachine.Path;
import com.epam.dokuchaiev.java.cashMachine.db.Status;
import com.epam.dokuchaiev.java.cashMachine.db.dao.OrderDAO;
import com.epam.dokuchaiev.java.cashMachine.db.entity.Receipt;
import com.epam.dokuchaiev.java.cashMachine.exceptions.AppException;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

public class CancelOrderCommand extends Command {
    private static final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss");
    private static final Logger LOGGER = Logger.getLogger(CancelOrderCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, AppException {
        LOGGER.debug("Command starts");

        HttpSession session = request.getSession(false);
        OrderDAO orderDAO = new OrderDAO();

        Receipt receipt =  orderDAO.findById(Integer.parseInt(String.valueOf(session.getAttribute("openedOrder"))));
        LOGGER.trace("Session attribute: openedOrder --> " + receipt);

        orderDAO.deleteOrder(receipt);
        LOGGER.trace("Delete order from DB: orderReceiptId --> " + receipt.getId());

        receipt.setTimestamp(new Timestamp(System.currentTimeMillis()));
        LOGGER.trace("Receipt timestamp updated: timestamp --> " + simpleDateFormat.format(receipt.getTimestamp()));

        receipt.setStatus(Status.CANCELED);
        LOGGER.trace("Receipt status updated: status --> " + receipt.getStatus());

        receipt.setTotalPrice(0.00);
        LOGGER.trace("Receipt total price updated: total price --> " + receipt.getTotalPrice());

        session.removeAttribute("openedOrder");
        LOGGER.trace("Session attribute: openedOrder --> " + session.getAttribute("openedOrder"));

        LOGGER.debug("Command finished");
        return Path.COMMAND_SHOW_ALL_RECEIPTS;
    }
}
