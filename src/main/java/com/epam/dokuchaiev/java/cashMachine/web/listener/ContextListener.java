package com.epam.dokuchaiev.java.cashMachine.web.listener;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class ContextListener implements ServletContextListener {
    private static final Logger LOGGER = Logger.getLogger(ContextListener.class);

    @Override
    public void contextInitialized(ServletContextEvent event) {
        LOGGER.debug("Servlet context initialization started");

        ServletContext context = event.getServletContext();
        initLog4j(context);
        initCommandContainer();

        LOGGER.debug("Servlet context initialization finished");
    }

    private void initLog4j(ServletContext servletContext) {
        LOGGER.debug("Log4j initialization started");
        try {
            PropertyConfigurator.configure(
                    servletContext.getRealPath("WEB-INF/log4j.properties"));
            LOGGER.debug("Log4j successfully initialized");
        } catch (Exception exception) {
            LOGGER.error("Failed to initialize Log4j");
            exception.printStackTrace();
        }
        LOGGER.debug("Log4j initialization finished");
    }

    private void initCommandContainer() {
        try {
            Class.forName("com.epam.dokuchaiev.java.cashMachine.web.command.CommandContainer");
        } catch (ClassNotFoundException classNotFoundException) {
            throw new IllegalStateException("Failed to initialize Command Container");
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        LOGGER.debug("Servlet context destruction started");
        LOGGER.debug("Servlet context destruction finished");
    }
}
