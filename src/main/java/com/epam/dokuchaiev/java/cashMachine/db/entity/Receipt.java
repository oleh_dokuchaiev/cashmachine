package com.epam.dokuchaiev.java.cashMachine.db.entity;

import com.epam.dokuchaiev.java.cashMachine.db.Status;
import com.epam.dokuchaiev.java.cashMachine.db.Payment;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class Receipt extends Entity {
    private static final DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
    private static final DateFormat timeFormat = new SimpleDateFormat("hh:mm:ss");

    private Timestamp timestamp;
    private String date;
    private String time;
    private double totalPrice;
    private User user;
    private Status status;
    private Payment payment = Payment.CASH;

    public void setStatus(Status status) {
        this.status = status;
    }

    public Status getStatus() {
        return status;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
        date = dateFormat.format(timestamp);
        time = timeFormat.format(timestamp);
    }

    public String getDate() {
        return date;
    }

    public String getTime() {
        return time;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    public Payment getPayment() {
        return payment;
    }

    public void setPayment(Payment payment) {
        this.payment = payment;
    }

    @Override
    public String toString() {
        return "Receipt: [id: " + getId() + ", userLogin: " + user.getLogin() + "] date/time: " + date + "/" + time + ", status: " + status + " totalPrice: " + totalPrice;
    }
}
