package com.epam.dokuchaiev.java.cashMachine.web.command;

import com.epam.dokuchaiev.java.cashMachine.Path;
import com.epam.dokuchaiev.java.cashMachine.db.bean.UserBean;
import com.epam.dokuchaiev.java.cashMachine.db.dao.OrderDAO;
import com.epam.dokuchaiev.java.cashMachine.db.dao.UserDAO;
import com.epam.dokuchaiev.java.cashMachine.db.entity.Receipt;
import com.epam.dokuchaiev.java.cashMachine.db.entity.User;
import com.epam.dokuchaiev.java.cashMachine.exceptions.AppException;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Timestamp;

public class CreateOrderCommand extends Command {
    private static final Logger LOGGER = Logger.getLogger(CreateOrderCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, AppException {
        LOGGER.debug("Command starts");

        HttpSession session = request.getSession(false);
        UserDAO userDAO = new UserDAO();
        OrderDAO orderDAO = new OrderDAO();

        User user = userDAO.findByName(((UserBean)session.getAttribute("user")).getLogin());
        Receipt receipt = orderDAO.createNewReceipt(new Timestamp(System.currentTimeMillis()), user);
        LOGGER.trace("New order created: receipt --> " + receipt);

        session.setAttribute("openedOrder", receipt.getId());
        LOGGER.trace("Set the session attribute: openedOrder --> " + receipt);

        request.setAttribute("receiptId", receipt.getId());
        LOGGER.trace("Set request attribute: receiptId --> " + receipt.getId());

        LOGGER.debug("Command finished");
        return Path.COMMAND_SHOW_ORDER;
    }
}
