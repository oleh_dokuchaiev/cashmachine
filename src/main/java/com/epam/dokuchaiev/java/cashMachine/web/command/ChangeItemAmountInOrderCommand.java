package com.epam.dokuchaiev.java.cashMachine.web.command;

import com.epam.dokuchaiev.java.cashMachine.Path;
import com.epam.dokuchaiev.java.cashMachine.db.dao.ItemDAO;
import com.epam.dokuchaiev.java.cashMachine.db.dao.OrderDAO;
import com.epam.dokuchaiev.java.cashMachine.db.entity.Item;
import com.epam.dokuchaiev.java.cashMachine.db.entity.Receipt;
import com.epam.dokuchaiev.java.cashMachine.exceptions.AppException;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class ChangeItemAmountInOrderCommand extends Command {
    private static final Logger LOGGER = Logger.getLogger(ChangeItemAmountInOrderCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, AppException {
        LOGGER.debug("Command starts");

        HttpSession session = request.getSession(false);
        OrderDAO orderDAO = new OrderDAO();
        Receipt receipt = orderDAO.findById(Integer.parseInt(String.valueOf(session.getAttribute("openedOrder"))));
        LOGGER.trace("Get session attribute: receipt --> " + receipt);

        String localeTag = String.valueOf(session.getAttribute("locale"));
        LOGGER.trace("Get session attribute: locale --> " + localeTag);

        int itemId = Integer.parseInt(request.getParameter("itemId"));
        LOGGER.trace("Request parameter: itemId --> " + itemId);
        Item item = new ItemDAO().findById(itemId);
        item.setAmount(Double.parseDouble(request.getParameter("itemAmount")));
        LOGGER.trace("Request parameter: amount --> " + item.getAmount());
        item.setPrice(orderDAO.calcPrice(item.getId(), item.getAmount()));
        LOGGER.trace("Request parameter: price --> " + item.getPrice());

        orderDAO.updateItemInOrder(receipt, item);
        LOGGER.trace("Item amount in order changed: order --> " + orderDAO.findOrderItemBeans(receipt.getId(), localeTag));

        LOGGER.debug("Command finished");
        return Path.COMMAND_SHOW_ORDER;
    }
}
