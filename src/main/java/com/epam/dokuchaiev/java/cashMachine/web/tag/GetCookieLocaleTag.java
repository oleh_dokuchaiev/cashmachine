package com.epam.dokuchaiev.java.cashMachine.web.tag;

import org.apache.log4j.Logger;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.tagext.TagSupport;

public class GetCookieLocaleTag extends TagSupport {
    private static final Logger LOGGER = Logger.getLogger(GetCookieLocaleTag.class);

    public int doStartTag() {
        LOGGER.debug("Tag starts");
        HttpServletRequest request = (HttpServletRequest) this.pageContext.getRequest();
        LOGGER.trace("Get request from pageContext: HttpServletRequest --> " + (request != null));
        if (request != null) {
            Cookie localeCookie = null;
            HttpSession session = request.getSession();
            Cookie[] cookies = request.getCookies();
            if (cookies != null) {
                for (Cookie cookie : cookies) {
                    if (cookie.getName().equals("CookedLocale")) {
                        localeCookie = cookie;
                    }
                }
            }
            LOGGER.trace("Get cookie: localeCookie --> " + localeCookie);
            if (localeCookie != null) {
                session.setAttribute("locale", localeCookie.getValue());
            } else {
                session.setAttribute("locale", "en");
            }
            LOGGER.trace("Set session attribute: locale --> " + session.getAttribute("locale"));
        }
        LOGGER.debug("Tag finished");
        return 0;
    }
}
