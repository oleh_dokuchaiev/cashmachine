package com.epam.dokuchaiev.java.cashMachine.web.command;

import com.epam.dokuchaiev.java.cashMachine.Path;
import com.epam.dokuchaiev.java.cashMachine.db.dao.ItemDAO;
import com.epam.dokuchaiev.java.cashMachine.db.entity.Item;
import com.epam.dokuchaiev.java.cashMachine.exceptions.AppException;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class CreateItemCommand extends Command {
    private static final Logger LOGGER = Logger.getLogger(CreateItemCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, AppException {
        LOGGER.debug("Command starts");

        String newItemNameRu = null;
        String newItemNameUa = null;

        String newItemName = request.getParameter("newItemName");
        LOGGER.trace("Get request parameter: newItemName --> " + newItemName);
        if (request.getParameter("newItemNameRu") != null) {
            newItemNameRu = readInput(String.valueOf(request.getParameter("newItemNameRu")));
            LOGGER.trace("Get request parameter: newItemNameRu --> " + newItemNameRu);
        }
        if (request.getParameter("newItemNameUa") != null) {
            newItemNameUa = readInput(String.valueOf(request.getParameter("newItemNameUa")));
            LOGGER.trace("Get request parameter: newItemNameUa --> " + newItemNameUa);
        }

        double newItemPrice = getPrice(request);
        LOGGER.trace("Get request parameter: newItemPrice --> " + newItemPrice);
        double newItemAmount = getAmount(request);
        LOGGER.trace("Get request parameter: newItemName --> " + newItemAmount);

        ItemDAO itemDAO = new ItemDAO();
        Item item = itemDAO.createNewItem(newItemName, newItemPrice, newItemAmount);
        LOGGER.trace("Created new item: item --> " + item.toString());

        if (newItemNameRu != null) {
            itemDAO.createNewItemLocale(item.getId(), newItemNameRu, "ru");
            LOGGER.trace("Created new item locale: locale --> ru, newItemNameRu --> " + newItemNameRu);
        }
        if (newItemNameUa != null) {
            itemDAO.createNewItemLocale(item.getId(), newItemNameUa, "ua");
            LOGGER.trace("Created new item locale: locale --> ua, newItemNameUa --> " + newItemNameUa);
        }

        LOGGER.debug("Command finished");
        return Path.COMMAND_SHOW_ALL_ITEMS;
    }

    private double getPrice(HttpServletRequest request) {
        String reqPrice = request.getParameter("newItemPrice");
        LOGGER.trace("Get request parameter: price --> " + reqPrice);
        if (reqPrice.contains(",")) {
            reqPrice = reqPrice.replace(",", ".");
            LOGGER.trace("Delimiter replaced to appropriate: reqPrice --> " + reqPrice);
        }
        double itemPrice = Double.parseDouble(reqPrice);
        LOGGER.trace("Set item parameter: itemPrice --> " + itemPrice);
        return itemPrice;
    }

    private double getAmount(HttpServletRequest request) {
        String reqAmount = request.getParameter("newItemAmount");
        LOGGER.trace("Get request parameter: amount --> " + reqAmount);
        if (reqAmount.contains(",")) {
            reqAmount = reqAmount.replace(",", ".");
            LOGGER.trace("Delimiter replaced to appropriate: reqAmount --> " + reqAmount);
        }
        double itemAmount = Double.parseDouble(reqAmount);
        LOGGER.trace("Set item parameter: itemAmount --> " + itemAmount);
        return itemAmount;
    }

    private String readInput(String input) {
        byte[] bytes = input.getBytes(StandardCharsets.UTF_8);
        return new String(bytes, StandardCharsets.UTF_8);
    }
}