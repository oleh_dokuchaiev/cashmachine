package com.epam.dokuchaiev.java.cashMachine.web.command;

import com.epam.dokuchaiev.java.cashMachine.Path;
import com.epam.dokuchaiev.java.cashMachine.db.bean.ItemBean;
import com.epam.dokuchaiev.java.cashMachine.db.dao.ItemDAO;
import com.epam.dokuchaiev.java.cashMachine.exceptions.AppException;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SearchItemCommand extends Command {
    private static final Logger LOGGER = Logger.getLogger(SearchItemCommand.class);
    private static final String REGEX_SEARCH_BY_ID = "^\\s?[0-9]+";
    private static final String REGEX_SEARCH_BY_NAME = "^\\s?[a-zA-Zа-яА-ЯёЁіІєЄїЇґҐ]+";

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, AppException {
        LOGGER.debug("Command starts");

        String forward = Path.PAGE_SEARCH_RESULTS;
        List<ItemBean> itemBeanList = new ArrayList<>();
        HttpSession session = request.getSession(false);

        String searchInput = readInput(String.valueOf(request.getParameter("search")));
        LOGGER.trace("Get request attribute: searchInput --> " + searchInput);
        int searchScope = getSearchScope(searchInput);
        LOGGER.trace("Get search scope: searchScope --> " + searchScope);

        switch (searchScope) {
            case (0): {
                itemBeanList = new ItemDAO().searchById(searchInput, String.valueOf(session.getAttribute("locale")));
                break;
            }
            case(1): {
                itemBeanList = new ItemDAO().searchByName(searchInput, String.valueOf(session.getAttribute("locale")));
                break;
            }
            case(2): {
                forward = Path.PAGE_ERROR_PAGE;
                String errorMessage = "Invalid input. Input should match item id or part of name";
                request.setAttribute("errorMessage", errorMessage);
                LOGGER.trace("Set request attribute: errorMessage --> " + errorMessage);
                break;
            }
        }

        request.setAttribute("searchInput", searchInput);
        LOGGER.trace("Set request attribute: searchInput --> " + request.getAttribute("searchInput"));

        request.setAttribute("searchResults", itemBeanList);
        LOGGER.trace("Set request attribute: searchResults --> " + request.getAttribute("searchResults").toString());

        LOGGER.debug("Command finished");
        return forward;
    }

    private int getSearchScope(String input) {
        int scope = 2;
        List<String> regexList = Arrays.asList(REGEX_SEARCH_BY_ID, REGEX_SEARCH_BY_NAME);
        for (String regex : regexList) {
            Pattern pattern = Pattern.compile(regex);
            Matcher matcher = pattern.matcher(input);
            if (matcher.find()) {
                scope = regexList.indexOf(regex);
            }
        }
        return scope;
    }

    private String readInput(String input) {
        byte[] bytes = input.getBytes(StandardCharsets.UTF_8);
        return new String(bytes, StandardCharsets.UTF_8);
    }
}
