package com.epam.dokuchaiev.java.cashMachine.web.command;

import com.epam.dokuchaiev.java.cashMachine.Path;
import com.epam.dokuchaiev.java.cashMachine.db.dao.ItemDAO;
import com.epam.dokuchaiev.java.cashMachine.db.dao.OrderDAO;
import com.epam.dokuchaiev.java.cashMachine.db.entity.Item;
import com.epam.dokuchaiev.java.cashMachine.db.entity.Receipt;
import com.epam.dokuchaiev.java.cashMachine.exceptions.AppException;
import com.epam.dokuchaiev.java.cashMachine.exceptions.DBException;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class RemoveItemFromOrderCommand extends Command {
    private static final Logger LOGGER = Logger.getLogger(RemoveItemFromOrderCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, AppException {
        LOGGER.debug("Command starts");

        HttpSession session = request.getSession(false);
        OrderDAO orderDAO = new OrderDAO();

        Receipt receipt = orderDAO.findById(Integer.parseInt(String.valueOf(session.getAttribute("openedOrder"))));

        String localeTag = String.valueOf(session.getAttribute("locale"));
        LOGGER.trace("Get session attribute: locale --> " + localeTag);

        Item item = new ItemDAO().findById(Integer.parseInt(request.getParameter("itemId")));
        LOGGER.trace("Get item from request: item --> " + item.toString());
        item.setAmount(Double.parseDouble(request.getParameter("itemOrderAmount")));
        LOGGER.trace("Set item amount to request parameter: itemAmount --> " + item.getAmount());
        item.setPrice(Double.parseDouble(request.getParameter("itemOrderPrice")));
        LOGGER.trace("Set item price to request parameter: itemPrice --> " + item.getPrice());

        orderDAO.removeItem(receipt, item);
        LOGGER.trace("Item removed from order: order --> " + orderDAO.findOrderItemBeans(receipt.getId(), localeTag));

        LOGGER.debug("Command finished");
        return Path.COMMAND_SHOW_ORDER;
    }
}
