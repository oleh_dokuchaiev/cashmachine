package com.epam.dokuchaiev.java.cashMachine.db.dao;

import com.epam.dokuchaiev.java.cashMachine.db.DBConnectionPool;
import com.epam.dokuchaiev.java.cashMachine.db.Fields;
import com.epam.dokuchaiev.java.cashMachine.db.bean.ItemBean;
import com.epam.dokuchaiev.java.cashMachine.db.entity.Item;
import com.epam.dokuchaiev.java.cashMachine.exceptions.DBException;
import com.epam.dokuchaiev.java.cashMachine.exceptions.Messages;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.*;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ItemDAO extends DAO {
    private static final Logger LOGGER = Logger.getLogger(ItemDAO.class);
    private static final Lock LOCK = new ReentrantLock();

    private static final String SQL_FIND_ITEM_BY_ID = "SELECT * FROM items WHERE id=?";
//    private static final String SQL_FIND_ITEM_BY_NAME = "SELECT * FROM items WHERE name=?";
//    private static final String SQL_FIND_ALL_ITEMS = "SELECT * FROM items";
    private static final String SQL_UPDATE_ITEM = "UPDATE items SET price=?, amount=? WHERE id=?";
    private static final String SQL_CREATE_NEW_ITEM = "INSERT INTO items VALUES(DEFAULT, ?, ?, ?)";
    private static final String SQL_CREATE_NEW_ITEM_LOCALE = "INSERT INTO items_translate VALUES(DEFAULT, ?, ?, ?)";
//    private static final String SQL_CHECK_ITEM_AMOUNT = "SELECT amount FROM items WHERE id=?";
//    private static final String SQL_GET_LANGUAGE_ID = "SELECT id FROM languages WHERE name=?";
    private static final String SQL_GET_LIMITED_RECORDS = "SELECT * FROM items limit ";
//    private static final String SQL_GET_LIMITED_TRANSLATED_RECORDS = "SELECT i.id, it.name, i.price, i.amount FROM items_translate it, items i WHERE it.items_id=i.id AND languages_id=? limit ";
    private static final String SQL_GET_NUMBER_OF_RECORDS = "SELECT COUNT(*) FROM items";
    private static final String SQL_GET_DEFAULT_ITEM_ID = "SELECT id FROM items WHERE name=?";
    private static final String SQL_FIND_TRANSLATED_ITEM_ID = "SELECT items_id FROM items_translate WHERE name=? AND languages_id=?";
    private static final String SQL_GET_DEFAULT_ITEM_NAME = "SELECT name FROM items WHERE id=?";
    private static final String SQL_GET_TRANSLATED_ITEM_NAME = "SELECT name FROM items_translate WHERE items_id=? AND languages_id=?";
    private static final String SQL_FIND_DEFAULT_ITEM_NAMES = "SELECT name FROM items";
    private static final String SQL_FIND_TRANSLATED_ITEM_NAMES = "SELECT name FROM items_translate WHERE languages_id=?";

//    private static final DBConnectionPool connectionPool = DBConnectionPool.getInstance();
    private static final DBConnectionPool connectionPool = initConnectionPool();

    private static DBConnectionPool initConnectionPool() {
        DBConnectionPool instance = null;
        try {
            instance = DBConnectionPool.getInstance();
        } catch (DBException dbException) {
            LOGGER.error(Messages.ERR_CANNOT_GET_CONNECTION_POOL_INSTANCE, dbException);
        }
        return instance;
    }

    @Override
    public Item findById(int id) throws DBException {
        Item item = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Connection connection = null;
        try {
            connection = connectionPool.getConnection();
            preparedStatement = connection.prepareStatement(SQL_FIND_ITEM_BY_ID);
            preparedStatement.setString(1, String.valueOf(id));
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                item = getItem(resultSet);
            }
            connection.commit();
        } catch (SQLException sqlException) {
            connectionPool.rollback(connection);
            throw new DBException(Messages.ERR_CANNOT_OBTAIN_ITEM_BY_ID, sqlException);
        } finally {
            connectionPool.closeResources(resultSet, preparedStatement);
            connectionPool.releaseConnection(connection);
        }
        return item;
    }

//    @Override
//    public Item findByName(String name) throws DBException {
//        Item item = null;
//        PreparedStatement preparedStatement = null;
//        ResultSet resultSet = null;
//        Connection connection = null;
//        try {
//            connection = connectionPool.getConnection();
//            preparedStatement = connection.prepareStatement(SQL_FIND_ITEM_BY_NAME);
//            preparedStatement.setString(1, name);
//            resultSet = preparedStatement.executeQuery();
//            if (resultSet.next()) {
//                item = getItem(resultSet);
//            }
//            connection.commit();
//        } catch (SQLException sqlException) {
//            LOGGER.error(Messages.ERR_CANNOT_OBTAIN_ITEM_BY_NAME, sqlException);
//            throw new DBException(Messages.ERR_CANNOT_OBTAIN_ITEM_BY_NAME, sqlException);
//        }
//        return item;
//    }
//
//    @Override
//    public List<Item> getEntities() throws DBException {
//        List<Item> itemList = new ArrayList<>();
//        PreparedStatement preparedStatement = null;
//        ResultSet resultSet = null;
//        Connection connection = connectionPool.getConnection();
//        try {
//            LOCK.lock();
//            preparedStatement = connection.prepareStatement(SQL_FIND_ALL_ITEMS);
//            resultSet = preparedStatement.executeQuery();
//            while (resultSet.next()) {
//                itemList.add(getItem(resultSet));
//            }
//            connection.commit();
//        } catch (SQLException sqlException) {
//            connectionPool.rollback(connection);
//            LOGGER.error(Messages.ERR_CANNOT_OBTAIN_LIST_OF_ITEMS, sqlException);
//            throw new DBException(Messages.ERR_CANNOT_OBTAIN_LIST_OF_ITEMS, sqlException);
//        } finally {
//            connectionPool.closeResources(resultSet, preparedStatement);
//            LOCK.unlock();
//            connectionPool.releaseConnection(connection);
//        }
//        return itemList;
//    }

    public List<ItemBean> getAllRecords(String localeTag, int start, int total) throws DBException {
        List<ItemBean> itemBeans = new ArrayList<>();
        int languageId = DAO.getLanguageId(localeTag);
        LOGGER.debug("Get language id: languageId --> " + languageId);
//        String sqlStatement = (languageId != 0) ? SQL_GET_LIMITED_TRANSLATED_RECORDS + (start-1) + ", " + total : SQL_GET_LIMITED_RECORDS + (start-1) + ", " + total;

        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        Connection connection = connectionPool.getConnection();
        try {
            LOCK.lock();
            preparedStatement = connection.prepareStatement(SQL_GET_LIMITED_RECORDS + (start-1) + ", " + total);
//            if (languageId != 0) {
//                preparedStatement.setInt(1, languageId);
//            }
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                itemBeans.add(getItemBean(resultSet, languageId));
            }
            connection.commit();
        } catch (SQLException sqlException) {
            connectionPool.rollback(connection);
            LOGGER.error(Messages.ERR_CANNOT_OBTAIN_LIMITED_LIST_OF_TRANSLATED_ITEMS, sqlException);
            throw new DBException(Messages.ERR_CANNOT_OBTAIN_LIMITED_LIST_OF_TRANSLATED_ITEMS, sqlException);
        } finally {
            connectionPool.closeResources(resultSet, preparedStatement);
            LOCK.unlock();
            connectionPool.releaseConnection(connection);
        }
        return itemBeans;
    }

    private ItemBean getItemBean(ResultSet resultSet, int languageId) throws SQLException, DBException {
        ItemBean bean = new ItemBean();
        int itemId = resultSet.getInt(Fields.ENTITY_ID);
        bean.setId(itemId);
        bean.setName(getItemName(itemId, languageId));
        bean.setPrice(resultSet.getDouble(Fields.ITEM_PRICE));
        bean.setAmount(resultSet.getDouble(Fields.ITEM_AMOUNT));
        return bean;
    }

    public ItemBean getItem(int itemId, String localeTag) throws DBException {
        ItemBean bean = new ItemBean();
        int languageId = DAO.getLanguageId(localeTag);
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        Connection connection = connectionPool.getConnection();
        try {
            LOCK.lock();
            preparedStatement = connection.prepareStatement(SQL_FIND_ITEM_BY_ID);
            preparedStatement.setInt(1, itemId);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                bean = getItemBean(resultSet, languageId);
            }
            connection.commit();
        } catch (SQLException sqlException) {
            connectionPool.rollback(connection);
            LOGGER.error(Messages.ERR_CANNOT_OBTAIN_ITEM_BY_ID);
            throw new DBException(Messages.ERR_CANNOT_OBTAIN_ITEM_BY_ID, sqlException);
        } finally {
            connectionPool.closeResources(resultSet, preparedStatement);
            LOCK.unlock();
            connectionPool.releaseConnection(connection);
        }
        return bean;
    }

    public static int getNumberOfItems() throws DBException {
        int result = 0;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Connection connection = connectionPool.getConnection();
        try {
            LOCK.lock();
            preparedStatement = connection.prepareStatement(SQL_GET_NUMBER_OF_RECORDS);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                result = resultSet.getInt(1);
            }
            connection.commit();
        } catch (SQLException sqlException) {
            connectionPool.rollback(connection);
            LOGGER.error(Messages.ERR_CANNOT_OBTAIN_NUMBER_OF_ALL_ITEMS);
            throw new DBException(Messages.ERR_CANNOT_OBTAIN_NUMBER_OF_ALL_ITEMS, sqlException);
        } finally {
            connectionPool.closeResources(resultSet, preparedStatement);
            LOCK.unlock();
            connectionPool.releaseConnection(connection);
        }
        return result;
    }

//    @Override
//    public int getLanguageId(String localeTag) throws DBException {
//        int languageId=0;
//        PreparedStatement preparedStatement = null;
//        ResultSet resultSet = null;
//        Connection connection = connectionPool.getConnection();
//        try {
//            preparedStatement = connection.prepareStatement(SQL_GET_LANGUAGE_ID);
//            preparedStatement.setString(1, localeTag);
//            resultSet = preparedStatement.executeQuery();
//            if (resultSet.next()) {
//                languageId = resultSet.getInt(Fields.ENTITY_ID);
//            }
//            connection.commit();
//        } catch (SQLException sqlException) {
//            LOGGER.error(Messages.ERR_CANNOT_OBTAIN_LANGUAGE_ID);
//            throw new DBException(Messages.ERR_CANNOT_OBTAIN_LANGUAGE_ID, sqlException);
//        }
//        return languageId;
//    }

    private static Item getItem(ResultSet resultSet) throws SQLException {
        Item item = new Item();
        item.setId(resultSet.getInt(Fields.ENTITY_ID));
        item.setName(resultSet.getString(Fields.ENTITY_NAME));
        item.setPrice(resultSet.getDouble(Fields.ITEM_PRICE));
        item.setAmount(resultSet.getDouble(Fields.ITEM_AMOUNT));
        return item;
    }

    public Item createNewItem(String name, double price, double amount) throws DBException {
        Item item = new Item();
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        Connection connection = connectionPool.getConnection();
        try {
            LOCK.lock();
            preparedStatement = connection.prepareStatement(SQL_CREATE_NEW_ITEM, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, name);
            preparedStatement.setDouble(2, amount);
            preparedStatement.setDouble(3, price);
            preparedStatement.executeUpdate();
            resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.next()) {
                item.setId(resultSet.getInt(1));
                item.setName(name);
                item.setPrice(price);
                item.setAmount(amount);
            }
            connection.commit();
        } catch (SQLException sqlException) {
            connectionPool.rollback(connection);
            LOGGER.error(Messages.ERR_CANNOT_CREATE_NEW_ITEM, sqlException);
            throw new DBException(Messages.ERR_CANNOT_CREATE_NEW_ITEM, sqlException);
        } finally {
            connectionPool.closeResources(resultSet, preparedStatement);
            LOCK.unlock();
            connectionPool.releaseConnection(connection);
        }
        return item;
    }

    public void createNewItemLocale(int itemId, String localeName, String localeTag) throws DBException {
        int languagesId = DAO.getLanguageId(localeTag);
        PreparedStatement preparedStatement = null;
        Connection connection = connectionPool.getConnection();
        try {
            LOCK.lock();
            preparedStatement = connection.prepareStatement(SQL_CREATE_NEW_ITEM_LOCALE);
            preparedStatement.setInt(1, languagesId);
            preparedStatement.setInt(2, itemId);
            preparedStatement.setString(3, localeName);
            preparedStatement.executeUpdate();
            connection.commit();
        } catch (SQLException sqlException) {
            connectionPool.rollback(connection);
            LOGGER.error(Messages.ERR_CANNOT_CREATE_NEW_ITEM_LOCALE);
            throw new DBException(Messages.ERR_CANNOT_CREATE_NEW_ITEM_LOCALE, sqlException);
        } finally {
            connectionPool.close(preparedStatement);
            LOCK.unlock();
            connectionPool.releaseConnection(connection);
        }
    }

    public void updateItem(Item item) throws DBException {
        Connection connection = connectionPool.getConnection();
        try {
            LOCK.lock();
            update(connection, item);
            connection.commit();
        } catch (SQLException sqlException) {
            connectionPool.rollback(connection);
            LOGGER.error(Messages.ERR_CANNOT_UPDATE_ITEM, sqlException);
            throw new DBException(Messages.ERR_CANNOT_UPDATE_ITEM, sqlException);
        } finally {
            LOCK.unlock();
            connectionPool.releaseConnection(connection);
        }
    }

    private void update(Connection connection, Item item) throws SQLException, DBException {
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(SQL_UPDATE_ITEM);
            preparedStatement.setDouble(1, item.getPrice());
            preparedStatement.setDouble(2, item.getAmount());
            preparedStatement.setInt(3, item.getId());
            preparedStatement.executeUpdate();
        } finally {
            connectionPool.close(preparedStatement);
        }
    }

//    public static boolean checkItem(Item item) throws DBException {
//        boolean result = false;
//        ResultSet resultSet = null;
//        PreparedStatement preparedStatement = null;
//        Connection connection = connectionPool.getConnection();
//        try {
//            LOCK.lock();
//            preparedStatement = connection.prepareStatement(SQL_CHECK_ITEM_AMOUNT);
//            preparedStatement.setInt(1, item.getId());
//            resultSet = preparedStatement.executeQuery();
//            if (resultSet.next()) {
//                double stockAmount = resultSet.getDouble("amount");
//                if (item.getAmount() <= stockAmount) {
//                    result = true;
//                }
//            }
//        } catch (SQLException sqlException) {
//            LOGGER.error(Messages.ERR_CANNOT_OBTAIN_ITEM_AMOUNT, sqlException);
//            throw new DBException(Messages.ERR_CANNOT_OBTAIN_ITEM_AMOUNT, sqlException);
//        } finally {
//            connectionPool.closeResources(resultSet, preparedStatement);
//            LOCK.unlock();
//            connectionPool.releaseConnection(connection);
//        }
//        return result;
//    }

    public List<ItemBean> searchById(String userInput, String localeTag) throws DBException {
        List<ItemBean> beans = new ArrayList<>();
//        int languageId = getLanguageId(localeTag);
//        ItemBean bean = new ItemBean().getBean(findById(Integer.parseInt(userInput)));
//        if (languageId != 0) {
//            bean.setName(getItemName(bean.getId(), languageId));
//        }
        ItemBean bean = getItem(Integer.parseInt(userInput), localeTag);
        beans.add(bean);
        return beans;
    }

    public List<ItemBean> searchByName(String userInput, String localeTag) throws DBException {
        List<ItemBean> beans = new ArrayList<>();
        int languageId = DAO.getLanguageId(localeTag);
        LOGGER.debug("Get language id: languageId --> " + languageId);
        List<String> names = new ArrayList<>();
        String sqlStatement = (languageId != 0) ? SQL_FIND_TRANSLATED_ITEM_NAMES : SQL_FIND_DEFAULT_ITEM_NAMES;
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        Connection connection = connectionPool.getConnection();
        try {
            LOCK.lock();
            preparedStatement = connection.prepareStatement(sqlStatement);
            if (languageId != 0) {
                preparedStatement.setInt(1, languageId);
            }
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                names.add(resultSet.getString(Fields.ENTITY_NAME));
            }
            for (String name : names) {
                if (name.contains(userInput)) {
//                    Item item;
//                    if (languageId != 0) {
//                         item = findById(getTranslatedItemId(name, languageId));
//                         item.setName(getItemName(item.getId(), languageId));
//                    } else {
//                        item = findByName(name);
//                    }
                    ItemBean bean = getItem(getItemId(name, languageId), localeTag);
//                    LOGGER.debug("Item: " + item.toString());
//                    ItemBean bean = new ItemBean().getBean(item);
                    beans.add(bean);
                }
            }
            connection.commit();
        } catch (SQLException sqlException) {
            connectionPool.rollback(connection);
            LOGGER.error(Messages.ERR_CANNOT_OBTAIN_LIMITED_LIST_OF_SEARCHED_ITEMS);
            throw new DBException(Messages.ERR_CANNOT_OBTAIN_LIMITED_LIST_OF_SEARCHED_ITEMS, sqlException);
        } finally {
            connectionPool.closeResources(resultSet, preparedStatement);
            LOCK.unlock();
            connectionPool.releaseConnection(connection);
        }
        return beans;
    }

    private int getItemId(String name, int languageId) throws DBException {
        int id = 0;
        String sqlStatement = (languageId != 0) ? SQL_GET_DEFAULT_ITEM_ID : SQL_FIND_TRANSLATED_ITEM_ID;
        String field = (languageId != 0) ? Fields.ORDER_ITEMS_ID : Fields.ENTITY_ID;
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        Connection connection = connectionPool.getConnection();
        try {
            LOCK.lock();
            preparedStatement = connection.prepareStatement(sqlStatement);
            preparedStatement.setString(1, name);
            if (languageId != 0) {
                preparedStatement.setInt(2, languageId);
            }
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                id = resultSet.getInt(field);
            }
            connection.commit();
        } catch (SQLException sqlException) {
            connectionPool.rollback(connection);
            LOGGER.error(Messages.ERR_CANNOT_OBTAIN_ITEM_ID);
            throw new DBException(Messages.ERR_CANNOT_OBTAIN_ITEM_ID, sqlException);
        } finally {
            connectionPool.closeResources(resultSet, preparedStatement);
            LOCK.unlock();
            connectionPool.releaseConnection(connection);
        }
        LOGGER.debug("Item id: " + id);
        return id;
    }

    protected String getItemName(int itemId, int languageId) throws DBException {
        String itemName;
        String sqlStatement = (languageId != 0) ? SQL_GET_TRANSLATED_ITEM_NAME : SQL_GET_DEFAULT_ITEM_NAME;
//        PreparedStatement preparedStatement = null;
//        ResultSet resultSet = null;
        Connection connection = connectionPool.getConnection();
        try {
            LOCK.lock();
            itemName = formatName(DAO.getName(connection, sqlStatement, itemId, languageId));
            LOGGER.debug("Get item name: itemName --> " + itemName);
            connection.commit();
        } catch (SQLException sqlException) {
            connectionPool.rollback(connection);
            LOGGER.error(Messages.ERR_CANNOT_OBTAIN_ITEM_NAME);
            throw new DBException(Messages.ERR_CANNOT_OBTAIN_ITEM_NAME, sqlException);
        } finally {
            LOCK.unlock();
            connectionPool.releaseConnection(connection);
        }
//        try {
//            LOCK.lock();
//            preparedStatement = connection.prepareStatement(sqlStatement);
//            preparedStatement.setInt(1, itemId);
//            if (languageId != 0) {
//                preparedStatement.setInt(2, languageId);
//            }
//            resultSet = preparedStatement.executeQuery();
//            if (resultSet.next()) {
//                name = resultSet.getString(Fields.ENTITY_NAME);
//            }
//            connection.commit();
//        } catch (SQLException sqlException) {
//            connectionPool.rollback(connection);
//            LOGGER.error(Messages.ERR_CANNOT_OBTAIN_ITEM_NAME, sqlException);
//            throw new DBException(Messages.ERR_CANNOT_OBTAIN_ITEM_NAME, sqlException);
//        } finally {
//            connectionPool.closeResources(resultSet, preparedStatement);
//            LOCK.unlock();
//            connectionPool.releaseConnection(connection);
//        }
        return itemName;
    }

    @Override
    public String formatName(String name) {
        name = Character.toUpperCase(name.charAt(0)) + name.substring(1);
        return name;
    }
}
