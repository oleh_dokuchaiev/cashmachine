package com.epam.dokuchaiev.java.cashMachine.web.command;

import com.epam.dokuchaiev.java.cashMachine.Path;
import com.epam.dokuchaiev.java.cashMachine.db.bean.UserBean;
import com.epam.dokuchaiev.java.cashMachine.exceptions.AppException;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashSet;

public class LogoutCommand extends Command {
    private static final Logger LOGGER = Logger.getLogger(LogoutCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, AppException {
        LOGGER.debug("Logout Command starts");

        HttpSession session = request.getSession(false);

        if (session != null) {
            String login = ((UserBean) session.getAttribute("user")).getLogin();
            LOGGER.trace("Get session attribute: login --> " + login);
            HashSet<String> loggedInUsers = (HashSet<String>) session.getServletContext().getAttribute("loggedInUsers");
            LOGGER.trace("Get context attribute: loggedInUsers --> " + loggedInUsers.toString());
            loggedInUsers.remove(login);
            session.getServletContext().setAttribute("loggedInUsers", loggedInUsers);
            LOGGER.trace("Set context attribute: loggedInUsers --> " + session.getServletContext().getAttribute("loggedInUsers").toString());
            session.invalidate();
        }

        LOGGER.debug("Logout Command finished");
        return Path.PAGE_INDEX;
    }
}
