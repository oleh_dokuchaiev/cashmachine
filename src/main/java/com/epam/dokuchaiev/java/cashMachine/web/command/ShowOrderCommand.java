package com.epam.dokuchaiev.java.cashMachine.web.command;

import com.epam.dokuchaiev.java.cashMachine.Path;
import com.epam.dokuchaiev.java.cashMachine.db.bean.ItemBean;
import com.epam.dokuchaiev.java.cashMachine.db.bean.ReceiptBean;
import com.epam.dokuchaiev.java.cashMachine.db.dao.OrderDAO;
import com.epam.dokuchaiev.java.cashMachine.exceptions.AppException;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

public class ShowOrderCommand extends Command {
    private static final Logger LOGGER = Logger.getLogger(ShowOrderCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, AppException {
        LOGGER.debug("Command starts");

        HttpSession session = request.getSession(false);
        String localeTag = String.valueOf(session.getAttribute("locale"));
        LOGGER.trace("Get session attribute: localeTag --> " + localeTag);

        int receiptId;
        if (request.getParameter("receiptId") == null) {
            receiptId = Integer.parseInt(String.valueOf(session.getAttribute("openedOrder")));
            LOGGER.trace("Get session attribute: receiptId --> " + receiptId);
        } else {
            receiptId = Integer.parseInt(request.getParameter("receiptId"));
            LOGGER.trace("Get request parameter: receiptId --> " + receiptId);
        }

        ReceiptBean receiptBean = new OrderDAO().getReceipt(receiptId, localeTag);
        List<ItemBean> orderItemBeanList = new OrderDAO().findOrderItemBeans(receiptId, localeTag);

        request.setAttribute("receipt", receiptBean);
        LOGGER.trace("Set request attribute: receipt --> " + receiptBean);
        request.setAttribute("orderItemList", orderItemBeanList);
        LOGGER.trace("Set request attribute: orderItemList --> " + orderItemBeanList);

        LOGGER.debug("Command finished");
        return Path.PAGE_ORDER;
    }
}
