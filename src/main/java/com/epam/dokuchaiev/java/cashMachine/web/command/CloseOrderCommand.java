package com.epam.dokuchaiev.java.cashMachine.web.command;

import com.epam.dokuchaiev.java.cashMachine.Path;
import com.epam.dokuchaiev.java.cashMachine.db.Payment;
import com.epam.dokuchaiev.java.cashMachine.db.Status;
import com.epam.dokuchaiev.java.cashMachine.db.dao.OrderDAO;
import com.epam.dokuchaiev.java.cashMachine.db.entity.Receipt;
import com.epam.dokuchaiev.java.cashMachine.exceptions.AppException;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

public class CloseOrderCommand extends Command {
    private static final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss");
    private static final Logger LOGGER = Logger.getLogger(CloseOrderCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, AppException {
        LOGGER.debug("Command starts");

        OrderDAO orderDAO = new OrderDAO();
        HttpSession session = request.getSession(false);
        Receipt receipt = orderDAO.findById(Integer.parseInt(String.valueOf(session.getAttribute("openedOrder"))));
        LOGGER.trace("Session attribute: openedOrder --> " + receipt);
        receipt.setPayment(Payment.valueOf(request.getParameter("payment")));
        LOGGER.trace("Get request parameter: payment --> " + receipt.getPayment());
        receipt.setTimestamp(new Timestamp(System.currentTimeMillis()));
        LOGGER.trace("Receipt timestamp updated: timestamp --> " + simpleDateFormat.format(receipt.getTimestamp()));

        receipt.setStatus(Status.PAID);
        LOGGER.trace("Receipt status updated: status --> " + receipt.getStatus());

        session.removeAttribute("openedOrder");
        LOGGER.trace("Remove the session attribute: openedOrder --> " + session.getAttribute("openedOrder"));

        orderDAO.closeOrder(receipt);

        LOGGER.debug("Command finished");
        return Path.PAGE_HOME;
    }
}
