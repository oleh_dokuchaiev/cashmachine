package com.epam.dokuchaiev.java.cashMachine.web.command;

import com.epam.dokuchaiev.java.cashMachine.Path;
import com.epam.dokuchaiev.java.cashMachine.db.bean.UserBean;
import com.epam.dokuchaiev.java.cashMachine.db.entity.Report;
import com.epam.dokuchaiev.java.cashMachine.exceptions.AppException;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Locale;

public class MakeXReportCommand extends Command {
    private static final Logger LOGGER = Logger.getLogger(MakeXReportCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, AppException {
        LOGGER.debug("Command starts");

        HttpSession session = request.getSession(false);

        String userLogin = ((UserBean)session.getAttribute("user")).getLogin();
        LOGGER.trace("Get current user login from session attribute: userLogin --> " + userLogin);
        Report xReport = Report.makeX(userLogin);

        request.setAttribute("xDate", xReport.getDate());
        LOGGER.trace("Set request attribute: xDate --> " + xReport.getDate());

        request.setAttribute("xTime", xReport.getTime());
        LOGGER.trace("Set request attribute: xTime --> " + xReport.getTime());

        request.setAttribute("xUserLogin", xReport.getUserLogin());
        LOGGER.trace("Set request attribute: xUserLogin --> " + xReport.getUserLogin());

        String xCash = String.format(Locale.US, "%.2f", xReport.getCashPayments());
        request.setAttribute("xCash", xCash);
        LOGGER.trace("Set request attribute: xCash --> " + xCash);

        String xTerminal = String.format(Locale.US, "%.2f", xReport.getTerminalPayments());
        request.setAttribute("xTerminal", xTerminal);
        LOGGER.trace("Set request attribute: xTerminal --> " + xTerminal);

        request.setAttribute("xTotalReceipts", xReport.getTotalReceipts());
        LOGGER.trace("Set request attribute: xTotalReceipts --> " + xReport.getTotalReceipts());

        request.setAttribute("xTotalSales", xReport.getTotalSales());
        LOGGER.trace("Set request attribute: xTotalSales --> " + xReport.getTotalSales());

        request.setAttribute("xTotalCancels", xReport.getTotalCancels());
        LOGGER.trace("Set request attribute: xTotalCancels --> " + xReport.getTotalCancels());

        request.setAttribute("xTotalCash", xReport.getTotalCash());
        LOGGER.trace("Set request attribute: xTotalCash --> " + xReport.getTotalCash());

        LOGGER.debug("Command finished");
        return Path.PAGE_X_REPORT;
    }
}
