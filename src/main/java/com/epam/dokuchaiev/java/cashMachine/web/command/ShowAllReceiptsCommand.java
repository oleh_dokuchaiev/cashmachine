package com.epam.dokuchaiev.java.cashMachine.web.command;

import com.epam.dokuchaiev.java.cashMachine.Path;
import com.epam.dokuchaiev.java.cashMachine.db.bean.ReceiptBean;
import com.epam.dokuchaiev.java.cashMachine.db.dao.OrderDAO;
import com.epam.dokuchaiev.java.cashMachine.exceptions.AppException;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.Serializable;
import java.util.*;

public class ShowAllReceiptsCommand extends Command {
    private static final Logger LOGGER = Logger.getLogger(ShowAllReceiptsCommand.class);

    private static class CompareById implements Comparator<ReceiptBean>, Serializable {
        @Override
        public int compare(ReceiptBean bean1, ReceiptBean bean2) {
            if (bean1.getId() > bean2.getId()) {
                return 1;
            } else {
                return -1;
            }
        }
    }

    private static final Comparator<ReceiptBean> compareById = new CompareById();

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, AppException {
        LOGGER.debug("Command starts");

        HttpSession session = request.getSession(false);
        String localeTag = (String.valueOf(session.getAttribute("locale")));
        LOGGER.trace("Get session attribute: locale --> " + localeTag);

        int page = 1;
        int start = 1;
        int total = 5;
        if (request.getParameter("page") != null) {
            page = Integer.parseInt(request.getParameter("page"));
        }
        if (page != 1) {
            start = page - 1;
            start = start * total + 1;
        }
        String pageForward = "?page=" + page;

        List<ReceiptBean> receiptList = new OrderDAO().getAllRecords(localeTag, start, total);
        LOGGER.trace("Extract from DB: receiptList --> " + receiptList);
        receiptList.sort(compareById);
        request.setAttribute("receiptList", receiptList);
        LOGGER.trace("Set the request attribute: receiptList --> " + receiptList.toString());

        int receiptsNum = new OrderDAO().getNumberOfReceipts();
        LOGGER.trace("Found in DB: number of all receipts --> " + receiptsNum);
        int pagesNum = receiptsNum / total;
        if (receiptsNum % total != 0) {
            ++pagesNum;
        }
        List<Integer> pages = new ArrayList<>();
        for (int i = 1; i <= pagesNum; i++) {
            pages.add(i);
        }
        request.setAttribute("pages", pages);
        LOGGER.trace("Set request attribute: pages --> " + pages.toString());

        LOGGER.debug("Command finished");
        return Path.PAGE_RECEIPT_LIST + pageForward;
    }
}
