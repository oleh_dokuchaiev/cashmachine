package com.epam.dokuchaiev.java.cashMachine.web.command;

import com.epam.dokuchaiev.java.cashMachine.Path;
import com.epam.dokuchaiev.java.cashMachine.db.dao.ItemDAO;
import com.epam.dokuchaiev.java.cashMachine.db.entity.Item;
import com.epam.dokuchaiev.java.cashMachine.exceptions.AppException;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class ChangeItemAmountInStorageCommand extends Command {
    private static final Logger LOGGER = Logger.getLogger(ChangeItemAmountInStorageCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, AppException {
        LOGGER.debug("Command starts");

        ItemDAO itemDAO = new ItemDAO();

        int itemId = Integer.parseInt(request.getParameter("itemId"));
        LOGGER.trace("Request parameter: itemId --> " + itemId);
        Item item = new ItemDAO().findById(itemId);
        item.setAmount(Double.parseDouble(request.getParameter("itemAmount")));
        LOGGER.trace("Request parameter: amount --> " + item.getAmount());
        item.setPrice(Double.parseDouble(request.getParameter("itemPrice")));
        LOGGER.trace("Request parameter: price --> " + item.getPrice());

        itemDAO.updateItem(item);
        LOGGER.trace("Item on storage updated: item --> " + item);

        LOGGER.debug("Command finished");
        return Path.COMMAND_SHOW_ALL_ITEMS;
    }
}
