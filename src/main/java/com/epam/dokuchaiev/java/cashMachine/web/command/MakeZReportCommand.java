package com.epam.dokuchaiev.java.cashMachine.web.command;

import com.epam.dokuchaiev.java.cashMachine.Path;
import com.epam.dokuchaiev.java.cashMachine.db.bean.ReceiptBean;
import com.epam.dokuchaiev.java.cashMachine.db.bean.UserBean;
import com.epam.dokuchaiev.java.cashMachine.db.entity.Report;
import com.epam.dokuchaiev.java.cashMachine.exceptions.AppException;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class MakeZReportCommand extends Command {
    private static final Logger LOGGER = Logger.getLogger(MakeZReportCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, AppException {
        LOGGER.debug("Command starts");

        HttpSession session = request.getSession(false);

        String userLogin = ((UserBean)session.getAttribute("user")).getLogin();
        LOGGER.trace("Get current user login from session attribute: userLogin --> " + userLogin);
        Report zReport = Report.makeZ(userLogin);

        request.setAttribute("zDate", zReport.getDate());
        LOGGER.trace("Set request attribute: zDate --> " + zReport.getDate());

        request.setAttribute("zTime", zReport.getTime());
        LOGGER.trace("Set request attribute: zTime --> " + zReport.getTime());

        request.setAttribute("zUserLogin", zReport.getUserLogin());
        LOGGER.trace("Set request attribute: zUserLogin --> " + zReport.getUserLogin());

        String xCash = String.format(Locale.US, "%.2f", zReport.getCashPayments());
        request.setAttribute("zCash", xCash);
        LOGGER.trace("Set request attribute: zCash --> " + xCash);

        String xTerminal = String.format(Locale.US, "%.2f", zReport.getTerminalPayments());
        request.setAttribute("zTerminal", xTerminal);
        LOGGER.trace("Set request attribute: zTerminal --> " + xTerminal);

        request.setAttribute("zTotalReceipts", zReport.getTotalReceipts());
        LOGGER.trace("Set request attribute: zTotalReceipts --> " + zReport.getTotalReceipts());

        request.setAttribute("zTotalSales", zReport.getTotalSales());
        LOGGER.trace("Set request attribute: zTotalSales --> " + zReport.getTotalSales());

        List<ReceiptBean> zSalesList = zReport.getSalesList();
        request.setAttribute("zSalesList", zSalesList);
        LOGGER.trace("Set request attribute: zSalesList --> " + zSalesList);

        request.setAttribute("zTotalCancels", zReport.getTotalCancels());
        LOGGER.trace("Set request attribute: zTotalCancels --> " + zReport.getTotalCancels());

        List<ReceiptBean> zCancelsList = zReport.getCancelsList();
        request.setAttribute("zCancelsList", zCancelsList);
        LOGGER.trace("Set request attribute: zCancelsList --> " + zCancelsList);

        request.setAttribute("zTotalCash", zReport.getTotalCash());
        LOGGER.trace("Set request attribute: zTotalCash --> " + zReport.getTotalCash());

        LOGGER.debug("Command finished");
        return Path.PAGE_Z_REPORT;
    }
}
