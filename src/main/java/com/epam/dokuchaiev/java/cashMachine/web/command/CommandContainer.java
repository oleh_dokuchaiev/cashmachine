package com.epam.dokuchaiev.java.cashMachine.web.command;

import org.apache.log4j.Logger;

import java.util.Map;
import java.util.TreeMap;

public class CommandContainer {

    private static final Logger LOGGER = Logger.getLogger(CommandContainer.class);

    private static Map<String, Command> commands = new TreeMap<>();

    static {
        /**
         * Common commands
         */
        commands.put("login", new LoginCommand());
        commands.put("logout", new LogoutCommand());
        commands.put("viewSettings", new ViewSettingsCommand());
        commands.put("changeLocale", new ChangeLocaleCommand());
        commands.put("noCommand", new NoCommand());
        commands.put("showAllItems", new ShowAllItemsCommand());
        commands.put("showOrder", new ShowOrderCommand());
        commands.put("showItem", new ShowItemCommand());
        commands.put("openMain", new OpenMainCommand());
        commands.put("searchItem", new SearchItemCommand());

        /**
         * Cashier commands
         */
        commands.put("createOrder", new CreateOrderCommand());
        commands.put("addItemToOrder", new AddItemToOrderCommand());
        commands.put("changeItemAmountInOrder", new ChangeItemAmountInOrderCommand());
        commands.put("closeOrder", new CloseOrderCommand());

        /**
         * Senior Cashier commands
         */
        commands.put("showAllReceipts", new ShowAllReceiptsCommand());
        commands.put("cancelOrder", new CancelOrderCommand());
        commands.put("removeItemFromOrder", new RemoveItemFromOrderCommand());
        commands.put("makeXReport", new MakeXReportCommand());
        commands.put("makeZReport", new MakeZReportCommand());

        /**
         * Commodity Expert commands
         */
        commands.put("createItem", new CreateItemCommand());
        commands.put("changeItemAmountInStorage", new ChangeItemAmountInStorageCommand());
        commands.put("showNewItem", new ShowNewItemCommand());

        LOGGER.debug("Command container was successfully initialized");
        LOGGER.trace("Number of commands --> " + commands.size());
    }

    public static Command get(String commandName) {
        if (commandName == null || !commands.containsKey(commandName)) {
            LOGGER.trace("Command '" + commandName + "'not found");
            return commands.get("noCommand");
        }
        return commands.get(commandName);
    }
}
