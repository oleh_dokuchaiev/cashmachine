package com.epam.dokuchaiev.java.cashMachine.exceptions;

public class DBException extends AppException {

    public DBException(String message, Throwable cause) {
        super(message, cause);
    }

}