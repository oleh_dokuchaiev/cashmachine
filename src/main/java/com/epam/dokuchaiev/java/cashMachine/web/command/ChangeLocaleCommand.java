package com.epam.dokuchaiev.java.cashMachine.web.command;

import com.epam.dokuchaiev.java.cashMachine.Path;
import com.epam.dokuchaiev.java.cashMachine.exceptions.AppException;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class ChangeLocaleCommand extends Command {
    private static final Logger LOGGER = Logger.getLogger(ChangeLocaleCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, AppException {
        LOGGER.debug("Command starts");
        HttpSession session = request.getSession(false);
        String locale = request.getParameter("locale");
        LOGGER.trace("Get request parameter: locale --> " + locale);

        session.setAttribute("locale", locale);
        LOGGER.trace("Set session attribute: locale --> " + locale);

        Cookie localeCookie;
        Cookie cookie = getCookie(request, "CookedLocale");

        if (cookie != null) {
            cookie.setValue(locale);
            localeCookie = cookie;
        } else {
            localeCookie = new Cookie("CookedLocale", locale);
        }
        response.addCookie(localeCookie);
        LOGGER.trace("Set cookie: CookedLocale --> " + localeCookie.getName() + "=" + localeCookie.getValue());

        LOGGER.debug("Command finished");
        return Path.PAGE_SETTINGS;
    }

    private static Cookie getCookie(HttpServletRequest request, String name) {
        if (request.getCookies() != null) {
            for (Cookie cookie : request.getCookies()) {
                if (cookie.getName().equals(name)) {
                    return cookie;
                }
            }
        }
        return null;
    }
}
