package com.epam.dokuchaiev.java.cashMachine.web.filter;

import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

public class EncodingFilter implements Filter {
    private static final Logger LOGGER = Logger.getLogger(EncodingFilter.class);

    private String encoding;

    @Override
    public void init(FilterConfig filterConfig) {
        LOGGER.debug("Filter initialization starts");
        encoding = filterConfig.getInitParameter("encoding");
        LOGGER.trace("Encoding from web.xml --> " + encoding);
        LOGGER.debug("Filter initialization finished");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        LOGGER.debug("Filter starts");

        HttpServletRequest httpRequest = (HttpServletRequest) request;
        LOGGER.trace("Request uri --> " + httpRequest.getRequestURI());

        String requestEncoding = request.getCharacterEncoding();
        if (requestEncoding == null) {
            LOGGER.trace("Request encoding is null. Setting encoding --> " + encoding);
            request.setCharacterEncoding(encoding);
        }

        if (!response.getCharacterEncoding().equals(encoding)) {
            response.setCharacterEncoding(encoding);
            LOGGER.trace("Set response character encoding: encoding --> " + encoding);
        }

        LOGGER.debug("Filter finished");
        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {
        LOGGER.debug("Filter destruction starts");

        LOGGER.debug("Filter destruction finished");
    }
}