package com.epam.dokuchaiev.java.cashMachine.web.command;

import com.epam.dokuchaiev.java.cashMachine.Path;
import com.epam.dokuchaiev.java.cashMachine.db.bean.UserBean;
import com.epam.dokuchaiev.java.cashMachine.db.dao.UserDAO;
import com.epam.dokuchaiev.java.cashMachine.db.Role;
import com.epam.dokuchaiev.java.cashMachine.db.entity.User;
import com.epam.dokuchaiev.java.cashMachine.exceptions.AppException;
import com.epam.dokuchaiev.java.cashMachine.exceptions.DBException;
import org.apache.log4j.Logger;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

public class LoginCommand extends Command {
    private static final Logger LOGGER = Logger.getLogger(LoginCommand.class);
    private static User user;
    private static String ERROR_MESSAGE;

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws AppException, ServletException, IOException {
        LOGGER.debug("Command starts");

        HttpSession session = request.getSession();
        String login = request.getParameter("login");
        LOGGER.trace("Get request parameter: login --> " + login);
        String password = request.getParameter("password");
        LOGGER.trace("Get request parameter: password --> " + password);

        String forward = Path.PAGE_ERROR_PAGE;

        if (!isCorrect(login, password)) {
            request.setAttribute("errorMessage", ERROR_MESSAGE);
            LOGGER.trace("Set request attribute: errorMessage --> " + ERROR_MESSAGE);
            LOGGER.debug("Command finished");
            return forward;
        }

        Role userRole = user.getRole();
        LOGGER.trace("User [" + login + "] role --> " + userRole);

        final List<Role> roles = Arrays.asList(Role.CASHIER, Role.SENIOR_CASHIER, Role.COMMODITY_EXPERT);
        if (roles.contains(userRole)) {
            forward = Path.COMMAND_OPEN_MAIN;
        }

        session.setAttribute("userRole", userRole);
        LOGGER.trace("Set the session attribute: userRole --> " + userRole);

        UserBean userBean = new UserBean();
        userBean.setId(user.getId());
        userBean.setLogin(user.getLogin());
        userBean.setFirstName(user.getFirstName());
        userBean.setLastName(user.getLastName());
        session.setAttribute("user", userBean);
        LOGGER.trace("Set the session attribute: user --> " + userBean);

        ServletContext context = request.getServletContext();
        HashSet<String> loggedInUsers = (HashSet<String>) context.getAttribute("loggedInUsers");
        if (loggedInUsers == null) {
            loggedInUsers = new HashSet<>();
        }
        LOGGER.trace("Get context attribute: loggedInUsers --> " + loggedInUsers.toString());
        loggedInUsers.add(login);
        context.setAttribute("loggedInUsers", loggedInUsers);
        LOGGER.trace("Set context attribute: loggedInUsers --> " + context.getAttribute("loggedInUsers").toString());

        LOGGER.info("User " + user + " logged as " + userRole.toString().toLowerCase());
        LOGGER.debug("Forward: " + forward);
        LOGGER.debug("Command finished");
        return forward;
    }

    private static boolean isCorrect(String login, String password) throws DBException {
        boolean isCorrect = true;
        if (login == null || login.isEmpty()) {
            ERROR_MESSAGE = "Login cannot be empty";
            isCorrect = false;
        }
        if (password == null || password.isEmpty()) {
            ERROR_MESSAGE = "Password cannot be empty";
            isCorrect = false;
        }
        user = new UserDAO().findByName(login);
        LOGGER.trace("Found in DB: user --> " + user);
        if (user == null) {
            ERROR_MESSAGE = "Invalid login";
            isCorrect = false;
        }

        if (!password.equals(user.getPassword())) {
            ERROR_MESSAGE = "Invalid password";
            isCorrect = false;
        }
        return isCorrect;
    }
}
