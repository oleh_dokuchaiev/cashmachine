package com.epam.dokuchaiev.java.cashMachine.db;

public enum Role {
    CASHIER, SENIOR_CASHIER, COMMODITY_EXPERT;

    public String getName() {
        return name().toLowerCase();
    }
}
