package com.epam.dokuchaiev.java.cashMachine.db.entity;

import com.epam.dokuchaiev.java.cashMachine.db.bean.ReceiptBean;
import com.epam.dokuchaiev.java.cashMachine.db.dao.OrderDAO;
import com.epam.dokuchaiev.java.cashMachine.exceptions.DBException;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

public class Report extends Entity {
    private static final DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
    private static final DateFormat timeFormat = new SimpleDateFormat("hh:mm:ss");

    private Timestamp timestamp = new Timestamp(System.currentTimeMillis());
    private String date = dateFormat.format(timestamp);
    private String time = timeFormat.format(timestamp);
    private String userLogin;
    private double cashPayments;
    private double terminalPayments;
    private int totalReceipts;
    private double totalCash;
    List<ReceiptBean> salesList;
    List<ReceiptBean> cancelsList;

    public List<ReceiptBean> getSalesList() {
        return salesList;
    }

    public void setSalesList(List<ReceiptBean> saleReceiptList) {
        this.salesList = saleReceiptList;
    }

    public List<ReceiptBean> getCancelsList() {
        return cancelsList;
    }

    public void setCancelsList(List<ReceiptBean> cancelsList) {
        this.cancelsList = cancelsList;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
        date = dateFormat.format(timestamp);
        time = timeFormat.format(timestamp);
    }

    public String getDate() {
        return date;
    }

    public String getTime() {
        return time;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    public double getCashPayments() {
        return cashPayments;
    }

    public void setCashPayments(double cashPayments) {
        this.cashPayments = cashPayments;
    }

    public double getTerminalPayments() {
        return terminalPayments;
    }

    public void setTerminalPayments(double terminalPayments) {
        this.terminalPayments = terminalPayments;
    }

    public int getTotalReceipts() {
        return totalReceipts;
    }

    public void setTotalReceipts(int totalReceipts) {
        this.totalReceipts = totalReceipts;
    }

    public int getTotalSales() {
        return salesList.size();
    }

    public int getTotalCancels() {
        return cancelsList.size();
    }


    public double getTotalCash() {
        return totalCash;
    }

    public void setTotalCash(double totalCash) {
        this.totalCash = totalCash;
    }

    public static Report makeX(String userLogin) throws DBException {
        return new OrderDAO().createReport(userLogin);
    }

    public static Report makeZ(String userLogin) throws DBException {
        OrderDAO orderDAO = new OrderDAO();
        Report report = orderDAO.createReport(userLogin);
        orderDAO.finishShift();
        return report;
    }
}
