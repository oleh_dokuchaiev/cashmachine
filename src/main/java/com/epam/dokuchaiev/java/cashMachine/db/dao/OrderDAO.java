package com.epam.dokuchaiev.java.cashMachine.db.dao;

import com.epam.dokuchaiev.java.cashMachine.db.DBConnectionPool;
import com.epam.dokuchaiev.java.cashMachine.db.Fields;
import com.epam.dokuchaiev.java.cashMachine.db.Status;
import com.epam.dokuchaiev.java.cashMachine.db.Payment;
import com.epam.dokuchaiev.java.cashMachine.db.bean.ItemBean;
import com.epam.dokuchaiev.java.cashMachine.db.bean.ReceiptBean;
import com.epam.dokuchaiev.java.cashMachine.db.entity.*;
import com.epam.dokuchaiev.java.cashMachine.exceptions.DBException;
import com.epam.dokuchaiev.java.cashMachine.exceptions.Messages;
import org.apache.log4j.Logger;

import java.sql.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class OrderDAO extends DAO {
    private static final Logger LOGGER = Logger.getLogger(OrderDAO.class);
    private static final Lock LOCK = new ReentrantLock();

    private static final String SQL_FIND_RECEIPT_BY_ID = "SELECT * FROM receipts WHERE id=?";
    private static final String SQL_GET_ALL_RECEIPTS = "SELECT * FROM receipts";
    private static final String SQL_GET_ALL_OPENED_ORDERS = "SELECT id FROM receipts WHERE statuses_id=1";
    private static final String SQL_UPDATE_RECEIPT = "UPDATE receipts SET datetime=?, total_price=?, payments_id=?, statuses_id=? WHERE id=?";
    private static final String SQL_CREATE_NEW_RECEIPT = "INSERT INTO receipts VALUES(DEFAULT, ?, DEFAULT, ?, 1, 1)";
    private static final String SQL_GET_ORDER_ITEMS = "SELECT items_id, item_amount, item_price FROM orders WHERE receipts_id=?";
    private static final String SQL_ADD_NEW_ITEM_TO_ORDER = "INSERT INTO orders VALUES(?, ?, ?, ?)";
    private static final String SQL_UPDATE_ITEM_AMOUNT_IN_ORDER = "UPDATE orders SET item_amount=?, item_price=? WHERE items_id=? AND receipts_id=?";
    private static final String SQL_GET_ITEM_FROM_ORDER = "SELECT item_amount, item_price FROM orders WHERE items_id=? AND receipts_id=?";
    private static final String SQL_DELETE_ITEM_FROM_ORDER = "DELETE FROM orders WHERE items_id=? AND receipts_id=?";
    private static final String SQL_GET_RECEIPT_BEAN_BY_ID = "SELECT r.id, r.datetime, r.total_price, u.login, r.statuses_id, r.payments_id FROM receipts r, users u WHERE r.users_id=u.id AND r.id=?";
    private static final String SQL_GET_DEFAULT_STATUS = "SELECT name FROM statuses WHERE id=?";
    private static final String SQL_GET_TRANSLATED_STATUS = "SELECT name FROM statuses_translate WHERE statuses_id=? AND languages_id=?";
    private static final String SQL_GET_DEFAULT_PAYMENT = "SELECT name FROM payments WHERE id=?";
    private static final String SQL_GET_TRANSLATED_PAYMENT = "SELECT name FROM payments_translate WHERE payments_id=? AND languages_id=?";
    private static final String SQL_GET_NUMBER_OF_RECORDS = "SELECT COUNT(*) FROM receipts";
    private static final String SQL_GET_LIMITED_RECORDS = "SELECT r.id, r.datetime, r.total_price, u.login, r.statuses_id, r.payments_id FROM receipts r, users u WHERE r.users_id=u.id limit ";
    private static final String SQL_DROP_ORDERS_TABLE = "DROP TABLE orders";
    private static final String SQL_TRUNCATE_RECEIPTS_TABLE = "TRUNCATE TABLE receipts";
    private static final String SQL_RECREATE_ORDERS = "CREATE TABLE orders(" +
            "receipts_id INTEGER NOT NULL REFERENCES receipts(id) ON DELETE CASCADE," +
            "items_id INTEGER NOT NULL REFERENCES items(id)," +
            "item_amount DECIMAL(19,3) NOT NULL DEFAULT 0.000," +
            "item_price DECIMAL(19,2) NOT NULL DEFAULT 0.00)";
    private static final String SQL_GET_ORDER_PRICES = "SELECT item_price FROM orders WHERE receipts_id=?";
    private static final String SQL_GET_ORDER_ITEMS_MAP = "SELECT items_id, item_amount FROM orders WHERE receipts_id=?";

    private static final DBConnectionPool connectionPool = initConnectionPool();

    private static DBConnectionPool initConnectionPool() {
        DBConnectionPool instance = null;
        try {
            instance = DBConnectionPool.getInstance();
        } catch (DBException dbException) {
            LOGGER.error(Messages.ERR_CANNOT_GET_CONNECTION_POOL_INSTANCE, dbException);
        }
        return instance;
    }

    public static int checkOpenedOrders() throws DBException {
        int result = 0;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Connection connection = connectionPool.getConnection();
        try {
            LOCK.lock();
            preparedStatement = connection.prepareStatement(SQL_GET_ALL_OPENED_ORDERS);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                result = resultSet.getInt(Fields.ENTITY_ID);
            }
            connection.commit();
        } catch (SQLException sqlException) {
            connectionPool.rollback(connection);
            LOGGER.error(Messages.ERR_CANNOT_OBTAIN_OPENED_ORDERS, sqlException);
            throw new DBException(Messages.ERR_CANNOT_OBTAIN_OPENED_ORDERS, sqlException);
        } finally {
            connectionPool.closeResources(resultSet, preparedStatement);
            LOCK.unlock();
            connectionPool.releaseConnection(connection);
        }
        return result;
    }

    @Override
    public Receipt findById(int id) throws DBException {
        Receipt receipt = new Receipt();
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Connection connection = null;
        try {
            connection = connectionPool.getConnection();
            preparedStatement = connection.prepareStatement(SQL_FIND_RECEIPT_BY_ID);
            preparedStatement.setString(1, String.valueOf(id));
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                receipt = getReceipt(resultSet);
            }
            connection.commit();
        } catch (SQLException sqlException) {
            LOGGER.error(Messages.ERR_CANNOT_OBTAIN_RECEIPT_BY_ID, sqlException);
            throw new DBException(Messages.ERR_CANNOT_OBTAIN_RECEIPT_BY_ID, sqlException);
        } finally {
            connectionPool.closeResources(resultSet, preparedStatement);
            connectionPool.releaseConnection(connection);
        }
        return receipt;
    }

    private Receipt getReceipt(ResultSet resultSet) throws DBException, SQLException {
        UserDAO userDAO = new UserDAO();
        Receipt receipt = new Receipt();
        receipt.setId(resultSet.getInt(Fields.ENTITY_ID));
        receipt.setTimestamp(resultSet.getTimestamp(Fields.RECEIPT_DATETIME));
        receipt.setTotalPrice(resultSet.getDouble(Fields.RECEIPT_TOTAL_PRICE));
        receipt.setUser(userDAO.findById(resultSet.getInt(Fields.RECEIPT_USERS_ID)));
        receipt.setStatus(Status.values()[resultSet.getInt(Fields.RECEIPT_STATUS_ID) - 1]);
        receipt.setPayment(Payment.values()[resultSet.getInt(Fields.RECEIPT_PAYMENT_ID) - 1]);
        return receipt;
    }

    private List<Receipt> getEntities() throws DBException {
        List<Receipt> receiptList = new ArrayList<>();
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Connection connection = connectionPool.getConnection();
        try {
            LOCK.lock();
            preparedStatement = connection.prepareStatement(SQL_GET_ALL_RECEIPTS);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                receiptList.add(getReceipt(resultSet));
            }
            connection.commit();
        } catch (SQLException sqlException) {
            connectionPool.rollback(connection);
            LOGGER.error(Messages.ERR_CANNOT_OBTAIN_LIST_OF_RECEIPTS, sqlException);
            throw new DBException(Messages.ERR_CANNOT_OBTAIN_LIST_OF_RECEIPTS, sqlException);
        } finally {
            connectionPool.closeResources(resultSet, preparedStatement);
            LOCK.unlock();
            connectionPool.releaseConnection(connection);
        }
        return receiptList;
    }

    public Receipt createNewReceipt(Timestamp timestamp, User user) throws DBException {
        Receipt receipt = new Receipt();
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        Connection connection = connectionPool.getConnection();
        try {
            LOCK.lock();
            preparedStatement = connection.prepareStatement(SQL_CREATE_NEW_RECEIPT, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setTimestamp(1, timestamp);
            preparedStatement.setInt(2, user.getId());
            preparedStatement.executeUpdate();
            resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.next()) {
                receipt.setId(resultSet.getInt(1));
                receipt.setTimestamp(timestamp);
                receipt.setUser(user);
                receipt.setTotalPrice(0.00);
            }
        } catch (SQLException sqlException) {
            connectionPool.rollback(connection);
            LOGGER.error(Messages.ERR_CANNOT_CREATE_NEW_RECEIPT, sqlException);
            throw new DBException(Messages.ERR_CANNOT_CREATE_NEW_RECEIPT, sqlException);
        } finally {
            connectionPool.closeResources(resultSet, preparedStatement);
            LOCK.unlock();
            connectionPool.releaseConnection(connection);
        }
        return receipt;
    }

    private void updateReceipt(Receipt receipt) throws DBException {
        LOGGER.debug("Receipt update started");
        Connection connection = connectionPool.getConnection();
        try {
            update(connection, receipt);
            connection.commit();
        } catch (SQLException sqlException) {
            connectionPool.rollback(connection);
            LOGGER.error(Messages.ERR_CANNOT_UPDATE_RECEIPT, sqlException);
            throw new DBException(Messages.ERR_CANNOT_UPDATE_RECEIPT, sqlException);
        } finally {
            connectionPool.releaseConnection(connection);
        }
        LOGGER.debug("Receipt update finished");
    }

    private void update(Connection connection, Receipt receipt) throws SQLException, DBException {
        PreparedStatement preparedStatement = null;
        int paymentId = receipt.getPayment().ordinal() + 1;
        LOGGER.debug("Get payment id: paymentId --> " + paymentId);
        int statusId = receipt.getStatus().ordinal() + 1;
        LOGGER.debug("Get status id: statusId --> " + statusId);
        try {
            preparedStatement = connection.prepareStatement(SQL_UPDATE_RECEIPT);
            preparedStatement.setTimestamp(1, receipt.getTimestamp());
            LOGGER.trace("Set parameter: timestamp --> " + receipt.getTimestamp());
            preparedStatement.setDouble(2, getReceiptTotalPrice(receipt.getId()));
            LOGGER.trace("Set parameter: totalPrice --> " + receipt.getTotalPrice());
            preparedStatement.setInt(3, paymentId);
            LOGGER.trace("Set parameter: payment --> " + receipt.getPayment().getName());
            preparedStatement.setInt(4, statusId);
            LOGGER.trace("Set parameter: status --> " + receipt.getStatus().getName());
            preparedStatement.setInt(5, receipt.getId());
            preparedStatement.executeUpdate();
        } finally {
            connectionPool.close(preparedStatement);
        }
    }

    private static Item getItem(Item itemInstance, double amount, double price) {
        Item item = new Item();
        item.setId(itemInstance.getId());
        item.setName(itemInstance.getName());
        item.setAmount(amount);
        item.setPrice(price);
        return item;
    }

    public Item findItemInOrder(int receiptId, Item item) throws DBException {
        Item itemRecord = new Item();
        itemRecord.setId(item.getId());
        itemRecord.setName(item.getName());
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        Connection connection = connectionPool.getConnection();
        try {
            LOCK.lock();
            preparedStatement = connection.prepareStatement(SQL_GET_ITEM_FROM_ORDER);
            preparedStatement.setInt(1, item.getId());
            preparedStatement.setInt(2, receiptId);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                itemRecord.setAmount(resultSet.getDouble(Fields.ORDER_ITEM_AMOUNT));
                itemRecord.setPrice(resultSet.getDouble(Fields.ORDER_ITEM_PRICE));
            }
            connection.commit();
        } catch (SQLException sqlException) {
            connectionPool.rollback(connection);
            LOGGER.error(Messages.ERR_CANNOT_OBTAIN_ITEM_FROM_ORDER, sqlException);
            throw new DBException(Messages.ERR_CANNOT_OBTAIN_ITEM_FROM_ORDER, sqlException);
        } finally {
            connectionPool.closeResources(resultSet, preparedStatement);
            LOCK.unlock();
            connectionPool.releaseConnection(connection);
        }
        return itemRecord;
    }

    public void addItem(Receipt receipt, Item item) throws DBException {
        Connection connection = connectionPool.getConnection();
        try {
            LOCK.lock();
            add(connection, receipt, item);
            updateItem(item, (item.getAmount() * -1));
            updateReceipt(receipt);
            connection.commit();
        } catch (SQLException sqlException) {
            connectionPool.rollback(connection);
            LOGGER.error(Messages.ERR_CANNOT_ADD_NEW_ITEM_TO_ORDER, sqlException);
            throw new DBException(Messages.ERR_CANNOT_ADD_NEW_ITEM_TO_ORDER, sqlException);
        } finally {
            LOCK.unlock();
            connectionPool.releaseConnection(connection);
        }
    }

    private void add(Connection connection, Receipt receipt, Item item) throws DBException, SQLException {
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(SQL_ADD_NEW_ITEM_TO_ORDER);
            preparedStatement.setInt(1, receipt.getId());
            preparedStatement.setInt(2, item.getId());
            preparedStatement.setDouble(3, item.getAmount());
            preparedStatement.setDouble(4, item.getPrice());
            preparedStatement.executeUpdate();
        } finally {
            connectionPool.close(preparedStatement);
        }
    }

    private void updateItem(Item item, double deltaAmount) throws DBException {
        ItemDAO itemDAO = new ItemDAO();
        Item stockItem = itemDAO.findById(item.getId());
        stockItem.setAmount(stockItem.getAmount() + deltaAmount);
        itemDAO.updateItem(stockItem);
    }

    public void updateItemInOrder(Receipt receipt, Item item) throws DBException {
        LOGGER.debug("Update item starts");
        if (item.getAmount() == 0) {
            removeItem(receipt, item);
        } else {
            Connection connection = connectionPool.getConnection();
            try {
                LOCK.lock();
                changeItemAmount(connection, item, receipt);
                updateReceipt(receipt);
                connection.commit();
            } catch (SQLException sqlException) {
                connectionPool.rollback(connection);
                LOGGER.error(Messages.ERR_CANNOT_UPDATE_ITEM_IN_ORDER, sqlException);
                throw new DBException(Messages.ERR_CANNOT_UPDATE_ITEM_IN_ORDER, sqlException);
            } finally {
                LOCK.unlock();
                connectionPool.releaseConnection(connection);
            }
        }
        LOGGER.debug("Update item finished");
    }

    private void changeItemAmount(Connection connection, Item item, Receipt receipt) throws DBException, SQLException {
        LOGGER.trace("Get parameter: itemCurrentState --> " + item);
        Item itemPrevState = findItemInOrder(receipt.getId(), item);
        LOGGER.trace("Get parameter: itemCurrentState --> " + item);
        LOGGER.trace("Get parameter: itemPrevState --> " + itemPrevState);
        double prevAmount = itemPrevState.getAmount();
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(SQL_UPDATE_ITEM_AMOUNT_IN_ORDER);
            preparedStatement.setDouble(1, item.getAmount());
            preparedStatement.setDouble(2, item.getPrice());
            preparedStatement.setInt(3, item.getId());
            preparedStatement.setInt(4, receipt.getId());
            preparedStatement.executeUpdate();
            updateItem(item, prevAmount - item.getAmount());
        } catch (SQLException sqlException) {
            LOGGER.error(Messages.ERR_CANNOT_CHANGE_ITEM_AMOUNT_IN_ORDER, sqlException);
            throw new SQLException(Messages.ERR_CANNOT_CHANGE_ITEM_AMOUNT_IN_ORDER, sqlException);
        } finally {
            connectionPool.close(preparedStatement);
        }
    }

    public void removeItem(Receipt receipt,  Item item) throws DBException {
        Connection connection = connectionPool.getConnection();
        try {
            LOCK.lock();
            remove(connection, receipt, item);
            updateReceipt(receipt);
            connection.commit();
        } catch (SQLException sqlException) {
            connectionPool.rollback(connection);
            LOGGER.error(Messages.ERR_CANNOT_REMOVE_ITEM_FROM_ORDER, sqlException);
            throw new DBException(Messages.ERR_CANNOT_REMOVE_ITEM_FROM_ORDER, sqlException);
        } finally {
            LOCK.unlock();
            connectionPool.releaseConnection(connection);
        }
    }

    private void remove(Connection connection, Receipt receipt, Item item) throws DBException, SQLException {
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(SQL_DELETE_ITEM_FROM_ORDER);
            preparedStatement.setInt(1, item.getId());
            preparedStatement.setInt(2, receipt.getId());
            preparedStatement.executeUpdate();
            updateItem(item, item.getAmount());
        } finally {
            connectionPool.close(preparedStatement);
        }
    }

    public void deleteOrder(Receipt receipt) throws DBException {
        List<Item> items = findOrderItems(String.valueOf(receipt.getId()));
        Connection connection = connectionPool.getConnection();
        try {
            LOCK.lock();
            for (Item item : items) {
                remove(connection, receipt, item);
            }
            updateReceipt(receipt);
            connection.commit();
        } catch (SQLException sqlException) {
            connectionPool.rollback(connection);
            LOGGER.error(Messages.ERR_CANNOT_DELETE_ORDER, sqlException);
            throw new DBException(Messages.ERR_CANNOT_DELETE_ORDER, sqlException);
        } finally {
            LOCK.unlock();
            connectionPool.releaseConnection(connection);
        }
    }

    public void closeOrder(Receipt receipt) throws DBException {
        Connection connection = connectionPool.getConnection();
        try {
            LOCK.lock();
            updateReceipt(receipt);
            connection.commit();
        } catch (SQLException sqlException) {
            connectionPool.rollback(connection);
            LOGGER.error(Messages.ERR_CANNOT_CLOSE_ORDER);
            throw new DBException(Messages.ERR_CANNOT_CLOSE_ORDER, sqlException);
        } finally {
            LOCK.unlock();
            connectionPool.releaseConnection(connection);
        }
    }

    private List<Item> findOrderItems(String receiptId) throws DBException {
        List<Item> content = new ArrayList<>();
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        Connection connection = connectionPool.getConnection();
        try {
            LOCK.lock();
            preparedStatement = connection.prepareStatement(SQL_GET_ORDER_ITEMS);
            preparedStatement.setString(1, receiptId);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                content.add(getItem((new ItemDAO().findById(resultSet.getInt(Fields.ORDER_ITEMS_ID))), resultSet.getDouble(Fields.ORDER_ITEM_AMOUNT), resultSet.getDouble(Fields.ORDER_ITEM_PRICE)));
            }
            connection.commit();
        } catch (SQLException sqlException) {
            connectionPool.rollback(connection);
            LOGGER.error(Messages.ERR_CANNOT_OBTAIN_ORDER_CONTENT, sqlException);
            throw new DBException(Messages.ERR_CANNOT_OBTAIN_ORDER_CONTENT, sqlException);
        } finally {
            connectionPool.closeResources(resultSet, preparedStatement);
            LOCK.unlock();
            connectionPool.releaseConnection(connection);
        }
        return content;
    }

    public Report createReport(String userLogin) throws DBException {
        Report report = new Report();
        List<Receipt> receiptList = new OrderDAO().getEntities();
        for (Receipt receipt : receiptList) {
            if (receipt.getStatus().equals(Status.OPENED)) {
                LOGGER.error(Messages.ERR_CANNOT_MAKE_REPORT);
                throw new RuntimeException("Opened orders found: " + receipt);
            }
        }
        report.setTimestamp(new Timestamp(System.currentTimeMillis()));
        report.setUserLogin(userLogin);
        report.setCashPayments(getIncome(receiptList, Payment.CASH));
        report.setTerminalPayments(getIncome(receiptList, Payment.TERMINAL));
        report.setTotalReceipts(receiptList.size());
        report.setTotalCash(report.getCashPayments() + report.getTerminalPayments());
        report.setSalesList(getReportBeans(receiptList, Status.PAID));
        report.setCancelsList(getReportBeans(receiptList, Status.CANCELED));
        return report;
    }

    private double getIncome(List<Receipt> receiptList, Payment payment) {
        double total = 0.00;
        for (Receipt receipt : receiptList) {
            if (receipt.getPayment().equals(payment)) {
                total += receipt.getTotalPrice();
            }
        }
        return total;
    }

    private static List<ReceiptBean> getReportBeans(List<Receipt> receiptList, Status status) {
        List<ReceiptBean> beans = new ArrayList<>();
        for (Receipt receipt : receiptList) {
            if (receipt.getStatus().equals(status)) {
                ReceiptBean bean = new ReceiptBean();
                bean.setId(receipt.getId());
                bean.setDate(receipt.getDate());
                bean.setTime(receipt.getTime());
                bean.setTotalPrice(receipt.getTotalPrice());
                beans.add(bean);
            }
        }
        return beans;
    }

    private double getReceiptTotalPrice(int receiptId) throws DBException {
        double totalPrice = 0.00;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Connection connection = connectionPool.getConnection();
        try {
            LOCK.lock();
            preparedStatement = connection.prepareStatement(SQL_GET_ORDER_PRICES);
            preparedStatement.setInt(1, receiptId);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                totalPrice += resultSet.getDouble(Fields.ORDER_ITEM_PRICE);
            }
            connection.commit();
        } catch (SQLException sqlException) {
            connectionPool.rollback(connection);
            LOGGER.error(Messages.ERR_CANNOT_OBTAIN_ITEM_PRICES_FROM_ORDER, sqlException);
            throw new DBException(Messages.ERR_CANNOT_OBTAIN_ITEM_PRICES_FROM_ORDER, sqlException);
        } finally {
            connectionPool.closeResources(resultSet, preparedStatement);
            LOCK.unlock();
            connectionPool.releaseConnection(connection);
        }
        return totalPrice;
    }

    public static Map<Integer, Double> getOrderItemsMap(int receiptId) throws DBException {
        Map<Integer, Double> orderItemsMap = new HashMap<>();
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Connection connection = connectionPool.getConnection();
        try {
            LOCK.lock();
            preparedStatement = connection.prepareStatement(SQL_GET_ORDER_ITEMS_MAP);
            preparedStatement.setInt(1, receiptId);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Integer key = resultSet.getInt(Fields.ORDER_ITEMS_ID);
                Double value = resultSet.getDouble(Fields.ORDER_ITEM_AMOUNT);
                orderItemsMap.put(key, value);
            }
            connection.commit();
        } catch (SQLException sqlException) {
            connectionPool.rollback(connection);
            LOGGER.error(Messages.ERR_CANNOT_OBTAIN_MAP_OF_ORDER_ITEMS, sqlException);
            throw new DBException(Messages.ERR_CANNOT_OBTAIN_MAP_OF_ORDER_ITEMS, sqlException);
        } finally {
            connectionPool.closeResources(resultSet, preparedStatement);
            LOCK.unlock();
            connectionPool.releaseConnection(connection);
        }
        return orderItemsMap;
    }

    public List<ReceiptBean> getAllRecords(String localeTag, int start, int total) throws DBException {
        List<ReceiptBean> receiptBeans = new ArrayList<>();
        int languageId = DAO.getLanguageId(localeTag);
        LOGGER.debug("Get language id: languageId --> " + languageId);
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        Connection connection = connectionPool.getConnection();
        try {
            LOCK.lock();
            preparedStatement = connection.prepareStatement(SQL_GET_LIMITED_RECORDS + (start - 1) + ", " + total);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                receiptBeans.add(getReceiptBean(resultSet, languageId));
            }
            connection.commit();
        } catch (SQLException sqlException) {
            connectionPool.rollback(connection);
            LOGGER.error(Messages.ERR_CANNOT_OBTAIN_LIMITED_LIST_OF_RECEIPTS, sqlException);
            throw new DBException(Messages.ERR_CANNOT_OBTAIN_LIMITED_LIST_OF_RECEIPTS, sqlException);
        } finally {
            connectionPool.closeResources(resultSet, preparedStatement);
            LOCK.unlock();
            connectionPool.releaseConnection(connection);
        }
        return receiptBeans;
    }

    private ReceiptBean getReceiptBean(ResultSet resultSet, int languageId) throws SQLException, DBException {
        ReceiptBean bean = new ReceiptBean();
        bean.setId(resultSet.getInt(Fields.ENTITY_ID));
        bean.setDate(getDate(resultSet.getTimestamp(Fields.RECEIPT_DATETIME)));
        bean.setTime(getTime(resultSet.getTimestamp(Fields.RECEIPT_DATETIME)));
        bean.setUserLogin(resultSet.getString(Fields.USER_LOGIN));
        bean.setTotalPrice(resultSet.getDouble(Fields.RECEIPT_TOTAL_PRICE));
        bean.setStatus(getStatus(resultSet.getInt(Fields.RECEIPT_STATUS_ID), languageId));
        bean.setPayment(getPayment(resultSet.getInt(Fields.RECEIPT_PAYMENT_ID), languageId));
        return bean;
    }

    public ReceiptBean getReceipt(int receiptId, String localeTag) throws DBException {
        ReceiptBean bean = new ReceiptBean();
        int languageId = DAO.getLanguageId(localeTag);
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        Connection connection = connectionPool.getConnection();
        try {
            LOCK.lock();
            preparedStatement = connection.prepareStatement(SQL_GET_RECEIPT_BEAN_BY_ID);
            preparedStatement.setInt(1, receiptId);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                bean = getReceiptBean(resultSet, languageId);
            }
            connection.commit();
        } catch (SQLException sqlException) {
            connectionPool.rollback(connection);
            LOGGER.error(Messages.ERR_CANNOT_OBTAIN_RECEIPT_BY_ID, sqlException);
            throw new DBException(Messages.ERR_CANNOT_OBTAIN_RECEIPT_BY_ID, sqlException);
        } finally {
            connectionPool.closeResources(resultSet, preparedStatement);
            LOCK.unlock();
            connectionPool.releaseConnection(connection);
        }
        return bean;
    }

    private String getDate(Timestamp timestamp) {
        final DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        return dateFormat.format(timestamp);
    }

    private String getTime(Timestamp timestamp) {
        final DateFormat timeFormat = new SimpleDateFormat("hh:mm:ss");
        return timeFormat.format(timestamp);
    }

    private String getStatus(int statusId, int languageId) throws DBException {
        String statusName;
        String sqlStatement = (languageId != 0) ? SQL_GET_TRANSLATED_STATUS : SQL_GET_DEFAULT_STATUS;
        Connection connection = connectionPool.getConnection();
        try {
            LOCK.lock();
            statusName = formatName(DAO.getName(connection, sqlStatement, statusId, languageId));
            LOGGER.debug("Get status name: statusName --> " + statusName);
            connection.commit();
        } catch (SQLException sqlException) {
            connectionPool.rollback(connection);
            LOGGER.error(Messages.ERR_CANNOT_OBTAIN_STATUS_NAME, sqlException);
            throw new DBException(Messages.ERR_CANNOT_OBTAIN_STATUS_NAME, sqlException);
        } finally {
            LOCK.unlock();
            connectionPool.releaseConnection(connection);
        }
        return statusName;
    }

    private String getPayment(int paymentId, int languageId) throws DBException {
        String paymentName;
        String sqlStatement = (languageId != 0) ? SQL_GET_TRANSLATED_PAYMENT : SQL_GET_DEFAULT_PAYMENT;
        Connection connection = connectionPool.getConnection();
        try {
            LOCK.lock();
            paymentName = formatName(DAO.getName(connection, sqlStatement, paymentId, languageId));
            LOGGER.debug("Get payment name: paymentName --> " + paymentName);
            connection.commit();
        } catch (SQLException sqlException) {
            connectionPool.rollback(connection);
            LOGGER.error(Messages.ERR_CANNOT_OBTAIN_PAYMENT_NAME, sqlException);
            throw new DBException(Messages.ERR_CANNOT_OBTAIN_PAYMENT_NAME, sqlException);
        } finally {
            LOCK.unlock();
            connectionPool.releaseConnection(connection);
        }
        return paymentName;
    }

    @Override
    public String formatName(String name) {
        StringBuilder result = new StringBuilder();
        if (name.contains("-") || name.contains(" ") || name.contains("_")) {
            String[] splitName = name.split("[-\\s_]");
            for (String split : splitName) {
                result.append(split.toUpperCase()).append(" ");
            }
            return result.toString().trim();
        } else {
            return name;
        }
    }

    public int getNumberOfReceipts() throws DBException {
        int result = 0;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Connection connection = connectionPool.getConnection();
        try {
            LOCK.lock();
            preparedStatement = connection.prepareStatement(SQL_GET_NUMBER_OF_RECORDS);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                result = resultSet.getInt(1);
            }
            connection.commit();
        } catch (SQLException sqlException) {
            connectionPool.rollback(connection);
            LOGGER.error(Messages.ERR_CANNOT_OBTAIN_NUMBER_OF_ALL_RECEIPTS);
            throw new DBException(Messages.ERR_CANNOT_OBTAIN_NUMBER_OF_ALL_RECEIPTS, sqlException);
        } finally {
            connectionPool.closeResources(resultSet, preparedStatement);
            LOCK.unlock();
            connectionPool.releaseConnection(connection);
        }
        return result;
    }

    public List<ItemBean> findOrderItemBeans(int receiptId, String localeTag) throws DBException {
        List<ItemBean> beanList = new ArrayList<>();
        int languageId = DAO.getLanguageId(localeTag);
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        Connection connection = connectionPool.getConnection();
        try {
            LOCK.lock();
            preparedStatement = connection.prepareStatement(SQL_GET_ORDER_ITEMS);
            preparedStatement.setInt(1, receiptId);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                beanList.add(extractItemBean(resultSet, languageId));
            }
            connection.commit();
        } catch (SQLException sqlException) {
            connectionPool.rollback(connection);
            LOGGER.error(Messages.ERR_CANNOT_OBTAIN_LIST_OF_ORDER_ITEM_BEANS, sqlException);
            throw new DBException(Messages.ERR_CANNOT_OBTAIN_LIST_OF_ORDER_ITEM_BEANS, sqlException);
        } finally {
            connectionPool.closeResources(resultSet, preparedStatement);
            LOCK.unlock();
            connectionPool.releaseConnection(connection);
        }
        return beanList;
    }

    private ItemBean extractItemBean(ResultSet resultSet, int languageId) throws SQLException, DBException {
        ItemBean itemBean = new ItemBean();
        int itemId = resultSet.getInt(Fields.ORDER_ITEMS_ID);
        itemBean.setId(itemId);
        itemBean.setName(new ItemDAO().getItemName(itemId, languageId));
        itemBean.setAmount(resultSet.getDouble(Fields.ORDER_ITEM_AMOUNT));
        itemBean.setPrice(resultSet.getDouble(Fields.ORDER_ITEM_PRICE));
        return itemBean;
    }

    public void finishShift() throws DBException {
        Connection connection = connectionPool.getConnection();
        try {
            LOCK.lock();
            dropOrders(connection);
            truncateReceipts(connection);
            recreateOrders(connection);
            connection.commit();
        } catch (SQLException sqlException) {
            connectionPool.rollback(connection);
            LOGGER.error(Messages.ERR_CANNOT_MAKE_REPORT, sqlException);
            throw new DBException(Messages.ERR_CANNOT_MAKE_REPORT, sqlException);
        } finally {
            LOCK.unlock();
            connectionPool.releaseConnection(connection);
        }
    }

    private void dropOrders(Connection connection) throws DBException {
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(SQL_DROP_ORDERS_TABLE);
            preparedStatement.executeUpdate();
        } catch (SQLException sqlException) {
            LOGGER.error(Messages.ERR_CANNOT_DROP_ORDERS_TABLE, sqlException);
            throw new DBException(Messages.ERR_CANNOT_DROP_ORDERS_TABLE, sqlException);
        } finally {
            connectionPool.close(preparedStatement);
        }
    }

    private void truncateReceipts(Connection connection) throws DBException {
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(SQL_TRUNCATE_RECEIPTS_TABLE);
            preparedStatement.executeUpdate();
        } catch (SQLException sqlException) {
            LOGGER.error(Messages.ERR_CANNOT_TRUNCATE_RECEIPTS_TABLE, sqlException);
            throw new DBException(Messages.ERR_CANNOT_TRUNCATE_RECEIPTS_TABLE, sqlException);
        } finally {
            connectionPool.close(preparedStatement);
        }
    }

    private void recreateOrders(Connection connection) throws DBException {
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(SQL_RECREATE_ORDERS);
            preparedStatement.executeUpdate();
        } catch (SQLException sqlException) {
            LOGGER.error(Messages.ERR_CANNOT_RECREATE_ORDERS_TABLE, sqlException);
            throw new DBException(Messages.ERR_CANNOT_RECREATE_ORDERS_TABLE, sqlException);
        } finally {
            connectionPool.close(preparedStatement);
        }
    }

    public double calcPrice(int itemId, double itemAmount) throws DBException {
        double itemPrice = new ItemDAO().findById(itemId).getPrice();
        return itemPrice * itemAmount;
    }
}
