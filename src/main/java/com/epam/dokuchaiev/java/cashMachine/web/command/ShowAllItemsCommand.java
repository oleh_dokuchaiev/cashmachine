package com.epam.dokuchaiev.java.cashMachine.web.command;

import com.epam.dokuchaiev.java.cashMachine.Path;
import com.epam.dokuchaiev.java.cashMachine.db.bean.ItemBean;
import com.epam.dokuchaiev.java.cashMachine.db.dao.ItemDAO;
import com.epam.dokuchaiev.java.cashMachine.exceptions.AppException;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.Serializable;
import java.util.*;

public class ShowAllItemsCommand extends Command {
    private static final Logger LOGGER = Logger.getLogger(ShowAllItemsCommand.class);

    private static class CompareById implements Comparator<ItemBean>, Serializable {

        @Override
        public int compare(ItemBean item1, ItemBean item2) {
            if (item1.getId() > item2.getId()) {
                return 1;
            } else {
                return -1;
            }
        }
    }

    private static final Comparator<ItemBean> compareById = new CompareById();

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, AppException {
        LOGGER.debug("Command starts");

        HttpSession session = request.getSession(false);
        String localeTag = (String.valueOf(session.getAttribute("locale")));
        LOGGER.trace("Get session attribute: locale --> " + localeTag);

        int page = 1;
        int start = 1;
        int total = 5;
        if (request.getParameter("page") != null) {
            page = Integer.parseInt(request.getParameter("page"));
        }
        if (page != 1) {
            start = page - 1;
            start = start * total + 1;
        }
        String pageForward = "?page=" + page;

        List<ItemBean> itemList = new ItemDAO().getAllRecords(localeTag, start, total);
        LOGGER.trace("Extract from DB: itemBeanList --> " + itemList);
        itemList.sort(compareById);
        request.setAttribute("itemList", itemList);
        LOGGER.trace("Set the request attribute: itemList --> " + itemList.toString());

        int itemsNum = ItemDAO.getNumberOfItems();
        LOGGER.trace("Found in DB: number of all items --> " + itemsNum);
        int pagesNum = itemsNum/total;
        if (itemsNum%total != 0) {
            ++pagesNum;
        }
        List<Integer> pages = new ArrayList<>();
        for (int i = 1; i <= pagesNum; i++) {
            pages.add(i);
        }
        request.setAttribute("pages", pages);
        LOGGER.trace("Set request attribute: pages --> " + pages.toString());

        LOGGER.debug("Command finished");
        return Path.PAGE_ITEM_LIST + pageForward;
    }
}
