package com.epam.dokuchaiev.java.cashMachine.db.entity;

import java.util.Locale;

public class Item extends Entity {
    private String name;
    private double price;
    private double amount;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getPrice() {
        return price;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public double getAmount() {
        return amount;
    }

    @Override
    public String toString() {
        return "Item [id: " + getId() + ", name: " + name + "] amount: " + String.format(Locale.US, "%.3f", amount) + ", price: " + String.format(Locale.US, "%.2f", price);
    }
}
