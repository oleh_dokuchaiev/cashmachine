package com.epam.dokuchaiev.java.cashMachine.db;

public enum Payment {
    CASH, TERMINAL;

    public String getName() {
        return name().toLowerCase();
    }
}
