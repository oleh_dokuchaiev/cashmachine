package com.epam.dokuchaiev.java.cashMachine.exceptions;

public class Messages {
    /**
     * Connection error messages
     */
    public static final String ERR_CANNOT_OBTAIN_CONNECTION = "Cannot obtain a connection from the pool";
    public static final String ERR_CANNOT_GET_CONNECTION_POOL_INSTANCE = "Cannot get connection pool instance";
    public static final String ERR_NO_AVAILABLE_CONNECTIONS = "Maximum pool size reached, no available connections";
    public static final String ERR_CANNOT_CREATE_CONNECTION = "Cannot create connection";
    public static final String ERR_CANNOT_ROLLBACK_TRANSACTION = "Cannot rollback transaction";
    public static final String ERR_CANNOT_CLOSE_RESOURCES = "Cannot close resources";
    public static final String ERR_CANNOT_CLOSE_STATEMENT = "Cannot close statement";
    public static final String ERR_CANNOT_CLOSE_RESULT_SET = "Cannot close resultSet";
    public static final String ERR_CANNOT_OBTAIN_DATA_SOURCE = "Cannot obtain the data source";

    /**
     * User error messages
     */
    public static final String ERR_CANNOT_OBTAIN_USER_BY_LOGIN = "Cannot obtain a user by its login";
    public static final String ERR_CANNOT_OBTAIN_USER_BY_ID = "Cannot obtain a user by its id";
    public static final String ERR_CANNOT_OBTAIN_USER_INFO_BEAN = "Cannot obtain user info bean";
    public static final String ERR_CANNOT_OBTAIN_ROLE_NAME = "Cannot obtain role name";

    /**
     * Item error messages
     */
    public static final String ERR_CANNOT_OBTAIN_ITEM_BY_ID = "Cannot obtain a product by its id";
    public static final String ERR_CANNOT_CREATE_NEW_ITEM = "Cannot create new item";
    public static final String ERR_CANNOT_CREATE_NEW_ITEM_LOCALE = "Cannot create new item locale";
    public static final String ERR_CANNOT_UPDATE_ITEM = "Cannot update item";
    public static final String ERR_CANNOT_OBTAIN_LANGUAGE_ID = "Cannot obtain language id";
    public static final String ERR_CANNOT_OBTAIN_LIMITED_LIST_OF_TRANSLATED_ITEMS = "Cannot obtain limited list of translated items";
    public static final String ERR_CANNOT_OBTAIN_NUMBER_OF_ALL_ITEMS = "Cannot obtain number of all items in stock";
    public static final String ERR_CANNOT_OBTAIN_ITEM_ID = "Cannot obtain item id";
    public static final String ERR_CANNOT_OBTAIN_ITEM_NAME = "Cannot obtain item name";
    public static final String ERR_CANNOT_OBTAIN_LIMITED_LIST_OF_SEARCHED_ITEMS = "Cannot obtain limited list of searched items";

    /**
     * Order error messages
     */
    public static final String ERR_CANNOT_OBTAIN_RECEIPT_BY_ID = "Cannot obtain a receipt by its id";
    public static final String ERR_CANNOT_OBTAIN_ITEM_PRICES_FROM_ORDER = "Cannot obtain item prices from order";
    public static final String ERR_CANNOT_OBTAIN_ORDER_CONTENT = "Cannot obtain content from order";
    public static final String ERR_CANNOT_OBTAIN_LIST_OF_ORDER_ITEM_BEANS = "Cannot obtain order item beans";
    public static final String ERR_CANNOT_OBTAIN_MAP_OF_ORDER_ITEMS = "Cannot obtain map of order items";
    public static final String ERR_CANNOT_CREATE_NEW_RECEIPT = "Cannot create new receipt";
    public static final String ERR_CANNOT_UPDATE_RECEIPT = "Cannot update receipt";
    public static final String ERR_CANNOT_ADD_NEW_ITEM_TO_ORDER = "Cannot add new item to order";
    public static final String ERR_CANNOT_UPDATE_ITEM_IN_ORDER = "Cannot update item in order";
    public static final String ERR_CANNOT_CHANGE_ITEM_AMOUNT_IN_ORDER = "Cannot change item amount in order";
    public static final String ERR_CANNOT_REMOVE_ITEM_FROM_ORDER = "Cannot remove item from order";
    public static final String ERR_CANNOT_OBTAIN_ITEM_FROM_ORDER = "Cannot obtain item from order";
    public static final String ERR_CANNOT_DELETE_ORDER = "Cannot delete order";
    public static final String ERR_CANNOT_OBTAIN_LIST_OF_RECEIPTS = "Cannot obtain list of receipts";
    public static final String ERR_CANNOT_MAKE_REPORT = "Cannot make report";
    public static final String ERR_CANNOT_OBTAIN_OPENED_ORDERS = "Cannot obtain opened orders";
    public static final String ERR_CANNOT_DROP_ORDERS_TABLE = "Cannot drop orders table";
    public static final String ERR_CANNOT_TRUNCATE_RECEIPTS_TABLE = "Cannot truncate receips table";
    public static final String ERR_CANNOT_RECREATE_ORDERS_TABLE = "Cannot recreate orders table";
    public static final String ERR_CANNOT_OBTAIN_STATUS_NAME = "Cannot obtain status name";
    public static final String ERR_CANNOT_OBTAIN_PAYMENT_NAME = "Cannot obtain payment name";
    public static final String ERR_CANNOT_OBTAIN_NUMBER_OF_ALL_RECEIPTS = "Cannot obtain number of all receipts";
    public static final String ERR_CANNOT_OBTAIN_LIMITED_LIST_OF_RECEIPTS = "Cannot obtain limited list of receipts";
    public static final String ERR_CANNOT_CLOSE_ORDER = "Cannot close order";

    /**
     * Web error messages
     */
    public static final String ERR_CANNOT_PRINT_OUT_DATE_FROM_TAG = "Cannot print out date from tag";
}
