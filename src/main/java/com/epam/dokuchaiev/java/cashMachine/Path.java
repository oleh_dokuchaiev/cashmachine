package com.epam.dokuchaiev.java.cashMachine;

public final class Path {
    /**
     * Pages
     */
    public static final String PAGE_INDEX = "/index.jsp";
    public static final String PAGE_HOME = "/WEB-INF/jsp/main.jsp";
    public static final String PAGE_LOGIN = "/login.jsp";
    public static final String PAGE_ERROR_PAGE = "/WEB-INF/jsp/error_page.jsp";
    public static final String PAGE_ITEM_LIST = "/WEB-INF/jsp/item_list.jsp";
    public static final String PAGE_ITEM = "/WEB-INF/jsp/item.jsp";
    public static final String PAGE_NEW_ITEM = "/WEB-INF/jsp/commodity-expert/new_item.jsp";
    public static final String PAGE_RECEIPT_LIST = "/WEB-INF/jsp/senior-cashier/receipt_list.jsp";
    public static final String PAGE_ORDER = "/WEB-INF/jsp/order.jsp";
    public static final String PAGE_X_REPORT = "/WEB-INF/jsp/senior-cashier/x_report.jsp";
    public static final String PAGE_Z_REPORT = "/WEB-INF/jsp/senior-cashier/z_report.jsp";
    public static final String PAGE_SETTINGS = "/WEB-INF/jsp/settings.jsp";
    public static final String PAGE_SEARCH_RESULTS = "/WEB-INF/jsp/search-results.jsp";

    /**
     * Commands
     */
    public static final String COMMAND_SHOW_ALL_RECEIPTS = "/controller?command=showAllReceipts";
    public static final String COMMAND_OPEN_MAIN = "/controller?command=openMain";
    public static final String COMMAND_SHOW_ORDER = "/controller?command=showOrder";
    public static final String COMMAND_SHOW_ALL_ITEMS = "/controller?command=showAllItems";
}
