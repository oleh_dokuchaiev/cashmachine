<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>
<html>
<head>
    <fmt:message key="page.itemList.title" var="title" scope="page"/>
    <%@ include file="/WEB-INF/jspf/head.jspf" %>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/style/list-page-style.css">
</head>
<body>
<%@ include file="/WEB-INF/jspf/header.jspf" %>
<div class="table-container">
    <h3 class="block-title"><fmt:message key="ui.itemList.table.title"/></h3>
    <hr>
    <div class="search-bar-block">
        <fmt:message key="ui.table.search" var="localeSearch"/>
        <c:url value="controller" var="searchItem">
            <c:param name="command" value="searchItem"/>
        </c:url>
        <form method="post" action="${searchItem}">
            <input type="text" class="search-bar" name="search" placeholder="${localeSearch}...">
        </form>
    </div>
    <div class="table-block">
        <table class="items">
            <thead>
            <tr>
                <th><fmt:message key="ui.table.header.id"/></th>
                <th><fmt:message key="ui.item.label.name.default"/></th>
                <th><fmt:message key="ui.item.label.amount"/></th>
                <th><fmt:message key="ui.item.label.price"/></th>
                <th><fmt:message key="ui.table.header.select"/></th>
            </tr>
            </thead>
            <tbody>
            <fmt:message key="ui.table.select" var="localeSubmit"/>
            <c:forEach var="result" items="${itemList}">
                <tr class="item-row">
                    <td class="item-id"><c:out value="${result.id}"/></td>
                    <td class="item-name"><c:out value="${result.name}"/></td>
                    <td class="item-amount"><c:out value="${result.amount}"/></td>
                    <td class="item-price"><c:out value="${result.price}"/></td>
                    <td class="select-row">
                        <c:url value="controller" var="showItem">
                            <c:param name="command" value="showItem"/>
                            <c:param name="itemId" value="${result.id}"/>
                            <c:param name="source" value="itemList"/>
                        </c:url>
                        <form class="select-item-form" method="post" action="${showItem}">
                            <input type="submit" class="select-row-button" value="${localeSubmit}"/>
                        </form>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
    <div class="pages-block">
        <c:forEach var="pageId" items="${pages}">
            <c:url value="controller" var="showNextPage">
                <c:param name="command" value="showAllItems"/>
                <c:param name="page" value="${pageId}"/>
            </c:url>
            <a <c:if test="${page == pageId}">class="active"</c:if>
                 href="${pageContext.request.contextPath}/controller?command=showAllItems&page=${pageId}">
                 ${pageId}
            </a>
        </c:forEach>
    </div>
</div>
<%@ include file="/WEB-INF/jspf/footer.jspf" %>
</body>
</html>