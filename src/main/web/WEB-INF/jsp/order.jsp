<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>

<html>
<head>
    <fmt:message key="page.order.title" var="title" scope="page"/>
    <%@ include file="/WEB-INF/jspf/head.jspf" %>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/style/action-page-style.css">
</head>
<body>
<jsp:include page="/WEB-INF/jspf/header.jspf"/>
<div class="main-container">
    <div class="info-block">
        <h3 class="block-title"><fmt:message key="ui.order.table.title"/> ${receiptId}</h3>
        <hr>
        <c:choose>
            <c:when test="${fn:length(orderItemList) == 0}">Empty order</c:when>
            <c:otherwise>
                <div id="items-block">
                    <table id="order-table">
                        <thead>
                        <tr>
                            <th><fmt:message key="ui.table.header.id"/></th>
                            <th><fmt:message key="ui.item.label.name.default"/></th>
                            <th><fmt:message key="ui.item.label.amount"/></th>
                            <th><fmt:message key="ui.item.label.price"/></th>
                            <th><fmt:message key="ui.table.header.select"/></th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach var="result" items="${orderItemList}">
                            <tr>
                                <td class="item-id"><c:out value="${result.id}"/></td>
                                <td class="item-name"><c:out value="${result.name}"/></td>
                                <td class="item-amount"><c:out value="${result.amount}"/></td>
                                <td class="item-price"><c:out value="${result.price}"/></td>
                                <td class="select-row-button">
                                    <c:url value="controller" var="showItem">
                                        <c:param name="command" value="showItem"/>
                                        <c:param name="itemId" value="${result.id}"/>
                                        <c:param name="itemOrderAmount" value="${result.amount}"/>
                                        <c:param name="itemOrderPrice" value="${result.price}"/>
                                        <c:param name="source" value="order"/>
                                    </c:url>
                                    <c:url value="controller" var="removeItem">
                                        <c:param name="command" value="removeItemFromOrder"/>
                                        <c:param name="itemId" value="${result.id}"/>
                                        <c:param name="itemName" value="${result.name}"/>
                                        <c:param name="itemOrderAmount" value="${result.amount}"/>
                                        <c:param name="itemOrderPrice" value="${result.price}"/>
                                    </c:url>
                                    <fmt:message key="ui.remove" var="localeRemove"/>
                                    <fmt:message key="ui.table.select" var="localeSelect"/>
                                    <c:choose>
                                        <c:when test="${userRole.name == 'senior_cashier' && status == 'opened'}">
                                            <form class="select-item-form" method="post" action="${removeItem}">
                                                <input type="submit" value="${localeRemove}"/>
                                            </form>
                                        </c:when>
                                        <c:otherwise>
                                            <form class="select-item-form" method="post" action="${showItem}">
                                                <input type="hidden" name="commandName">
                                                <input type="submit" value="${localeSelect}"
                                                       <c:if test="${userRole.name == 'senior_cashier' && status == 'paid'}">disabled</c:if>/>
                                            </form>
                                        </c:otherwise>
                                    </c:choose>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
                <div id="total-price-block">
                    <div id="total-price-message">
                        <h3><fmt:message key="ui.order.totalPrice.message"/>:</h3>
                    </div>
                    <div id="total-price-value">
                        <h3>${receipt.totalPrice}</h3>
                    </div>
                </div>
                <br>
            </c:otherwise>
        </c:choose>
    </div>
    <div class="action-block">
        <h3 class="user-menu-block"><fmt:message key="ui.main.menuBlock.title"/></h3>
        <hr>
        <c:choose>
            <c:when test="${userRole.name == 'cashier'}">
                <div class="order-option" id="add-item-block">
                    <c:url value="controller" var="showItemList">
                        <c:param name="command" value="showAllItems"/>
                    </c:url>
                    <form id="add-item-form" method="post" action="${showItemList}">
                        <button type="submit" class="menu-option-button" form="add-item-form"><fmt:message
                                key="ui.order.menuBlock.add"/></button>
                    </form>
                </div>
                <div class="order-option" id="close-order-block">
                    <c:url value="controller" var="closeOrder">
                        <c:param name="command" value="closeOrder"/>
                    </c:url>
                    <form id="payment-form" method="post" action="${closeOrder}">
                        <p>Select payment method</p>
                        <input type="radio" id="cash" name="payment" value="CASH" checked>
                        <label for="cash"><fmt:message key="base.payment.cash"/></label><br>
                        <input type="radio" id="terminal" name="payment" value="TERMINAL">
                        <label for="terminal"><fmt:message key="base.payment.terminal"/></label><br>
                        <button type="submit" class="menu-option-button" value="Submit" form="payment-form">
                            <fmt:message key="ui.order.menuBlock.close"/>
                        </button>
                    </form>
                </div>
            </c:when>
            <c:when test="${userRole.name == 'senior_cashier'}">
                <div class="order_option" id="cancel-order-block">
                    <c:url value="controller" var="cancelOrder">
                        <c:param name="command" value="cancelOrder"/>
                    </c:url>
                    <form id="cancel-order-form" method="post" action="${cancelOrder}">
                        <button type="submit" class="menu-option-button" value="Cancel" form="cancel-order-form">
                            <fmt:message
                                    key="ui.order.menuBlock.cancel"/></button>
                    </form>
                </div>
            </c:when>
        </c:choose>
    </div>
</div>
<jsp:include page="/WEB-INF/jspf/footer.jspf"/>
</body>
</html>