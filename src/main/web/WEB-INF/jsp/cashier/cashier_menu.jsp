<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>
<div class="action-block" id="user-menu-block">
    <h3 class="block-title"><fmt:message key="ui.main.menuBlock.title"/></h3>
    <hr>
    <div class="menu-option" id="create-order-option">
        <c:url value="controller" var="createOrder">
            <c:param name="command" value="createOrder"/>
        </c:url>
        <form id="create-order-form" method="post" action="${createOrder}">
            <button type="submit" class="menu-option-button" id="createOrderButton" form="create-order-form"
                    <c:if test="${not empty openedOrder}">
                        disabled
                    </c:if>>
                <fmt:message key="ui.main.menuBlock.newOrder"/>
            </button>
        </form>
    </div>
    <div class="menu-option" id="show-order-option">
        <c:url value="controller" var="showOrder">
            <c:param name="command" value="showOrder"/>
        </c:url>
        <form id="show-order-form" method="post" action="${showOrder}">
            <button type="submit" class="menu-option-button" id="showOrderButton" form="show-order-form"
                    <c:if test="${empty openedOrder}">
                        disabled
                    </c:if>>
                <fmt:message key="ui.main.menuBlock.currentOrder"/>
            </button>
        </form>
    </div>
</div>