<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>
<html>
<head>
    <fmt:message key="page.newItem.title" var="title"/>
    <%@ include file="/WEB-INF/jspf/head.jspf" %>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/style/action-page-style.css">
</head>
<body>
<jsp:include page="/WEB-INF/jspf/header.jspf"/>
<div id="item-creation-container">
    <c:url value="controller" var="createItem">
        <c:param name="command" value="createItem"/>
    </c:url>
    <h3 class="block-title"><fmt:message key="ui.newItem.title"/></h3>
    <hr>
    <fmt:message key="ui.submit" var="localeSubmit"/>
    <div class="input-block">
        <form id="new-item-form" method="post" action="${createItem}">
            <label for="new-item-name"><fmt:message key="ui.item.label.name.default"/></label><br>
            <input type="text" class="user-input" id="new-item-name" name="newItemName" placeholder="ItemName..."><br>
            <label for="new-item-name-ru"><fmt:message key="ui.item.label.name.ru"/></label><br>
            <input type="text" class="user-input" id="new-item-name-ru" name="newItemNameRu" placeholder="ItemName..."><br>
            <label for="new-item-name-ua"><fmt:message key="ui.item.label.name.ua"/></label><br>
            <input type="text" class="user-input" id="new-item-name-ua" name="newItemNameUa" placeholder="ItemName..."><br>
            <label for="new-item-price"><fmt:message key="ui.item.label.price"/></label><br>
            <input type="number" class="user-input" id="new-item-price" name="newItemPrice" step="0.01" min="0.00" placeholder="0.00"><br>
            <label for="new-item-amount"><fmt:message key="ui.item.label.amount"/></label><br>
            <input type="number" class="user-input" id="new-item-amount" name="newItemAmount" step="0.001" min="0.000" placeholder="0.000"><br>
            <hr>
            <input type="submit" class="loginButton" value="${localeSubmit}">
        </form>
    </div>
</div>
<jsp:include page="/WEB-INF/jspf/footer.jspf"/>
</body>
</html>
