<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>
<%@ taglib prefix="cl" uri="/WEB-INF/cookieLocale.tld" %>
<cl:GetCookieLocale/>
<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="messages" scope="session"/>
<html>
<head>
    <fmt:message key="page.settings.title" var="title" scope="page"/>
    <%@ include file="/WEB-INF/jspf/head.jspf" %>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/style/single-block-page-style.css">
</head>
<body>
<%@ include file="/WEB-INF/jspf/header.jspf" %>
<div class="single-container" id="settings-container">
    <h2 class="block-title"><fmt:message key="ui.settings.localeBlock.title"/></h2>
    <hr>
    <fmt:message key="ui.submit" var="localeSubmit"/>
    <form id="locale-form" action="controller" method="post">
        <input type="hidden" name="command" value="changeLocale"/>
        <div class="locale-option">
            <div class="locale-option-radio">
                <label class="locale-option-label">
                    <input type="radio" class="settings-option-radio" name="locale" value="en"
                           <c:if test="${locale == 'en'}">checked</c:if>/>
                    <img src="${pageContext.request.contextPath}/style/img/flag_uk.png">
                </label>
            </div>
            <div class="locale-option-name">
                <span><fmt:message key="ui.settings.option.english"/></span>
            </div>
        </div>
        <div class="locale-option">
            <div class="locale-option-radio">
                <label class="locale-option-label">
                    <input type="radio" class="settings-option-radio" name="locale" value="ru"
                           <c:if test="${locale == 'ru'}">checked</c:if>/>
                    <img src="${pageContext.request.contextPath}/style/img/flag_ru.png">
                </label>
            </div>
            <div class="locale-option-name">
                <span><fmt:message key="ui.settings.option.russian"/></span>
            </div>
        </div>
        <div class="locale-option">
            <div class="locale-option-radio">
                <label class="locale-option-label">
                    <input type="radio" class="settings-option-radio" name="locale" value="ukr"
                           <c:if test="${locale == 'ukr'}">checked</c:if>/>
                    <img src="${pageContext.request.contextPath}/style/img/flag_ua.png">
                </label>
            </div>
            <div class="locale-option-name">
                <span><fmt:message key="ui.settings.option.ukrainian"/></span>
            </div>
        </div>
        <input type="submit" class="submit-button" value="${localeSubmit}">
        <br>
    </form>
</div>
<%@ include file="/WEB-INF/jspf/footer.jspf" %>
</body>
</html>